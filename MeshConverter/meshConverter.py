#!/usr/bin/env python3.7


import pymesh
import sys
import trimesh
import numpy as np
from util.io import *
from util.mesh import *

if __name__ == "__main__":
    if (len(sys.argv) == 2):
        filepath = sys.argv[1]
        mesh = pymesh.load_mesh(filepath)
        tol = 15.0
        print("Read mesh with number of vertices:", mesh.num_vertices)
        # Increase resolution
        mesh, _ = pymesh.split_long_edges(mesh, tol)
        
        # Decrease resolution
        mesh, _ = pymesh.collapse_short_edges(mesh,7.0)
        
        # Construct tetrahedralization
        mesh = pymesh.tetrahedralize(mesh, 7.0, engine="tetgen")
        
        # IO module storing to specified Vega formats.
        # Maintains original mesh file.
        print("Storing new mesh with number of vertices:",mesh.num_vertices)
        pymesh.save_mesh_raw(filepath[:-4] + ".msh", mesh.vertices, mesh.faces, mesh.voxels, ascii=True)
        mesh_writer(mesh, filepath[:-4] + "_volume.obj")
        mesh_write_volume_homogeneous(mesh, filepath[:-4] + "-volumetric-homogeneous.veg")
        mesh_write_volume(mesh, filepath[:-4] + "-volumetric.obj")
        #pymesh.save_mesh(filepath[:-4] + ".obj", mesh, ascii=True)
        print (filepath)
    else:
        print ("Provide relative or complete path to mesh file, e.g. ./meshConverter.py /path/to/mesh.obj")
        print ("Alternatively specify which python to use by pythonX.X meshConverter.py /path/to/mesh.obj")
