#!/usr/bin/env python3.7


import pymesh
import sys
import trimesh
import numpy as np
from util.io import *
from util.mesh import *

if __name__ == "__main__":
    if (len(sys.argv) == 2):
        filepath = sys.argv[1]
        mesh = pymesh.load_mesh(filepath)
        # # for surface mesh:
        # mesh = pymesh.form_mesh(vertices, faces)

        # # for volume mesh:
        # mesh = pymesh.form_mesh(vertices, faces, voxels)
        
        mesh = pymesh.convex_hull(mesh)
        inflated_vertices = inflate(mesh, 3)
        mesh = pymesh.form_mesh(inflated_vertices, mesh.faces)

        tol = 20.0
        mesh, _ = pymesh.split_long_edges(mesh, tol)
        
        # # print("Read mesh with number of vertices:", mesh.num_vertices)

        #mesh = pymesh.refine_triangulation(mesh)
        
        #mesh, info = pymesh.remove_duplicated_faces(mesh)
        #print(mesh.num_vertices)
        
        # mesh, _ = pymesh.collapse_short_edges(mesh,5.0, preserve_feature=True)
        # mesh = pymesh.subdivide(mesh, order=1, method="simple")
        # mesh, _ = pymesh.collapse_short_edges(mesh,5.0, preserve_feature=True)

        #mesh = pymesh.subdivide(mesh, order=1, method="simple")
        # print(mesh.attribute_names)
        # print(mesh.get_attribute('face_sources'))
        
        tetgen = pymesh.tetgen()
        tetgen.points = mesh.vertices
        tetgen.triangles = mesh.faces
        tetgen.max_tet_volume = 7.0
        tetgen.verbosity = 1
        #tetgen.keep_convex_hull = True
        tetgen.run()
        mesh = tetgen.mesh
        #mesh = pymesh.tetrahedralize(mesh, 5.0, engine="tetgen")
        print(mesh.voxels)
        exit()
        print("Storing new mesh with number of vertices:",mesh.num_vertices)
        pymesh.save_mesh_raw(filepath[:-4] + ".msh", mesh.vertices, mesh.faces, mesh.voxels, ascii=True)
        mesh_writer(mesh, filepath[:-4] + "_volume.obj")
        mesh_write_volume_homogeneous(mesh, filepath[:-4] + "-volumetric-homogeneous.veg")
        mesh_write_volume(mesh, filepath[:-4] + "-volumetric.obj")
        #pymesh.save_mesh(filepath[:-4] + ".obj", mesh, ascii=True)
        print (filepath)
    else:
        print ("Provide relative or complete path to mesh file, e.g. ./" + sys.argv[0] + " /path/to/mesh.obj")
        print ("Alternatively specify which python to use by pythonX.X " + sys.argv[0] + " /path/to/mesh.obj")
