#!/usr/bin/env python3.7
import pymesh
import sys
import trimesh
import numpy as np
import os
from util.io import *
from util.mesh import *
import subprocess

if __name__ == "__main__":
    if (len(sys.argv) == 2):
        #volume_mesh = pymesh.load_mesh(sys.argv[1])
        #surface_mesh = pymesh.load_mesh(sys.argv[1])
        path = sys.argv[1].split('/')[:-1]
        path = "/".join(path) + "/"
        proc = subprocess.Popen(["ls "+ path], stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate()
        try:
            for i in out.split():
                if (".stl" not in str(i) and
                    ".bou" not in str(i)):
                    proc = subprocess.Popen(["rm -f " + path + str(i)[1:]], stdout=subprocess.PIPE, shell=True)
        except:
            pass

        os.system('tetgen -qa151.0 ' + sys.argv[1])
        
        fileprefix = sys.argv[1][:-4]
        vertices = read_tetgen_nodes(fileprefix + ".1.node")
        vertices = rotate_multiple(vertices, [2,1], [130,20])
        vertices = vertices - np.min(vertices, 0) - 20
        
        faces = read_tetgen_faces(fileprefix + ".1.face")
        voxels = read_tetgen_elements(fileprefix + ".1.ele")
        mesh = pymesh.form_mesh(vertices[1:], faces, voxels)
        print(len(mesh.voxels==0))
        
        mesh = vertex_normals(mesh)
        
        #print (mesh.get_attribute("vertex_normals"))
        # Write to Vega file format .veg
        mesh_write_volume_homogeneous(mesh, fileprefix + "-volumetric-homogeneous.veg")
        # Write to .Obj file format in ascii
        #mesh_writer(mesh, fileprefix + ".obj")

        
        pymesh.save_mesh_raw(fileprefix+"1.obj", mesh.vertices, faces, ascii=True)
    
        #pymesh.save_mesh(fileprefix+".obj", mesh, use_float=True)
        mesh_write_volume(mesh, fileprefix + "-volumetric.obj")

        # Store as pymesh format
        #pymesh.save_mesh_raw(fileprefix + ".msh", mesh.vertices, mesh.faces, mesh.voxels, ascii=True)

    else:
        print ("OBS one might want to run the ./meshCageGenerator.py prior to this script")
        print ("Provide relative or complete path to mesh file, e.g. ./" + sys.argv[0] +
               " /path/to/volume_mesh.msh /path/to/surface_mesh.obj")
        print ("Alternatively specify which python to use by pythonX.X " + sys.argv[0] +
               " /path/to/volume_mesh.msh /path/to/surface_mesh.obj")
