import numpy as np
import pymesh

def inflate(mesh, ratio=2):
    mesh_center = np.mean(mesh.vertices,0)
    directions = mesh.vertices - mesh_center
    lengths = np.linalg.norm(directions, axis=1)
    unit_directions = np.divide(directions,np.reshape(lengths,(len(lengths), 1)))
    return mesh.vertices + unit_directions*ratio

def rotate_vertices(vertices, axis=0, degrees=90):
    # |cos θ   −sin θ   0| |x|   |x cos θ − y sin θ|   |x'|
    # |sin θ    cos θ   0| |y| = |x sin θ + y cos θ| = |y'|
    # |  0       0      1| |z|   |        z        |   |z'|
    theta_radians = np.radians(degrees)
    s, c = np.sin(theta_radians),np.cos(theta_radians)
    if axis == 0:
        R = np.array(((c, -s, 0), (s, c, 0), (0, 0, 1)))
    elif axis == 1:
        # | cos θ    0   sin θ| |x|   | x cos θ + z sin θ|   |x'|
        # |   0      1       0| |y| = |         y        | = |y'|
        # |−sin θ    0   cos θ| |z|   |−x sin θ + z cos θ|   |z'|
        R = np.array(((c, 0, s), (0, 1, 0), (-s, 0, c)))
    elif axis == 2:
        # |1     0           0| |x|   |        x        |   |x'|
        # |0   cos θ    −sin θ| |y| = |y cos θ − z sin θ| = |y'|
        # |0   sin θ     cos θ| |z|   |y sin θ + z cos θ|   |z'|
        R = np.array(((1, 0, 0), (0, c, -s), (0, s, c)))
        
    return vertices.dot(R)

def rotate_multiple(vertices, axes=[], degrees=[]):
    for axis, degree in zip(axes, degrees):
        vertices = rotate_vertices(vertices, axis, degree)
    return vertices

def vertex_normals(mesh):
    mesh.add_attribute("vertex_normals")
    mesh.add_attribute("face_normals")
   
    vnorms = np.zeros((mesh.num_vertices,3))

    mesh.set_attribute("face_normals", np.zeros((mesh.num_faces,3)))

    for i in mesh.faces:
        face = mesh.vertices[i-1]
        p = np.cross(face[1] - face[0], face[2]-face[0])
        vnorms[i[0]] += p
        vnorms[i[1]] += p
        vnorms[i[2]] += p

    n = np.linalg.norm(vnorms, axis=1, keepdims=True)
    vnorms = np.divide(vnorms, n, out=np.zeros_like(vnorms), where=n!=0)
    mesh.set_attribute("vertex_normals", vnorms)
    vnorms = mesh.get_attribute("vertex_normals")
    return mesh

    
