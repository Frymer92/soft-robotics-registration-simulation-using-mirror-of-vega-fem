import pymesh
import numpy as np

def write_line(f, starter, elements):
    f.write(starter)
    for i in elements:
        f.write(" " + str(i))
    f.write("\n")

def mesh_writer(mesh, filename):
    with open(filename, 'w+') as f:
        f.write("#Generated with Pymesh writer\n\n")
        f.write("g default\n")
        for elem in mesh.vertices:
            write_line(f, "v", elem)
        for elem in mesh.faces:
            write_line(f, "f", elem)
        # for elem in mesh.voxels:
        #     write_line(f, "f", elem)

def mesh_write_volume_homogeneous(mesh, filename):
    with open(filename, 'w+') as f:
        f.write("# Vega mesh file.\n")
        f.write("# " + str(len(mesh.vertices)) + " vertices, " + str(len(mesh.voxels)) + " elements\n\n")
        f.write("*VERTICES\n")
        f.write(str(len(mesh.vertices)) + " 3 0 0\n")
        # Writes vertices to .veg format
        for idx, elem in enumerate(mesh.vertices):
            write_line(f, str(idx+1), elem)
        f.write("\n*ELEMENTS\n")
        f.write("TET\n")
        f.write(str(len(mesh.voxels)) + " 4 0\n")
        # Writes the tets
        for idx, elem in enumerate(mesh.voxels):
            write_line(f, str(idx+1), elem)
        f.write("\n*MATERIAL defaultMaterial\n")
        f.write("ENU, 100, 2500, 0.3\n\n")
        f.write("*REGION\n")
        f.write("allElements, defaultMaterial\n")            
        
def mesh_write_volume(mesh, filename):
    with open(filename, 'w+') as f:
        f.write("# Generated by the objfile class\n")
        f.write("# Number of vertices: " + str(len(mesh.vertices)) + "\n")
        f.write("# Number of texture coordinates: 0\n")
        f.write("# Number of normals: " + str(len(mesh.vertices)) +  "\n")
        f.write("# Number of faces: " + str(len(mesh.faces)) + "\n")
        f.write("# Number of groups: 1\n")
        # Write vertex
        for idx, elem in enumerate(mesh.vertices):
            write_line(f, "v", elem)

        # Write vertex normals
        for idx, elem in enumerate(np.resize(mesh.get_attribute("vertex_normals"),(-1,3))):
            write_line(f, "vn", elem)
        f.write("g Default\n")
        # Write faces
        for elem in mesh.faces:
            f.write("f ")
            for i in elem:
                f.write(str(i) + "//" + str(i) + " ")
            f.write("\n")
        # for elem in mesh.voxels:
        #     f.write("f ")
        #     for i in elem:
        #         f.write(str(i) + "//" + str(i) + " ")
        #     f.write("\n")


def read_tetgen_nodes(filepath):
    with open(filepath, 'r') as f:
        line = f.readline()
        arr = []
        while line:
            details = line.strip().split()

            if details[0] != "#":
                arr.append(np.asarray([float(details[1]), float(details[2]), float(details[3])]))
            line = f.readline()            
        return np.asarray(arr)

def read_tetgen_faces(filepath):
    with open(filepath, 'r') as f:
        line = f.readline()
        arr = []
        while line:
            details = line.strip().split()
            if details[0] != "#" and len(details) == 5:
                arr.append(np.array([int(details[1]), int(details[2]), int(details[3])]))
            line = f.readline()            
        return np.asarray(arr)

def read_tetgen_elements(filepath):
    with open(filepath, 'r') as f:
        line = f.readline()
        arr = []
        while line:
            details = line.strip().split()
            if details[0] != "#" and len(details) == 5:
                arr.append(np.array([int(details[1]), int(details[2]), int(details[3]), int(details[4])]))
            line = f.readline()            
        return np.asarray(arr)

def interp_write_ascii(interp, filename):
    with open(filename, 'w+') as f:
        f.write("lol")
