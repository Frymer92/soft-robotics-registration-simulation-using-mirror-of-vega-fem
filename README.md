### DISCLAIMER ###
This repository contains a vast majority of unoriginal code, originating from the Vega FEM V4.0 [http://run.usc.edu/vega/](http://run.usc.edu/vega/). It is copyrighted under the [3-clause BSD lisence](https://en.wikipedia.org/wiki/BSD_licenses).

This repository extends and compliments vast parts of code included in
the original Vega FEM v4.0, however changes and modifications are only
related to a computer science master's thesis project.  I emphasize
that I am not claiming this project to pure original, and have only
made circumstantial modifications to the Vega FEM v4.0.  Most
modifications are embedded into what is called the
softRoboticsSimulator. Note that this driver is strongly inspired at
top level of the interactiveDeformableSimulator, that comes with the
original Vega FEM v4.0 project. And it used to facilitate the research
of performing marker-less soft body tracking.

This software is also under the [3-clause BSD license](https://opensource.org/licenses).

Copyright 2019 Mathias Grymer

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
