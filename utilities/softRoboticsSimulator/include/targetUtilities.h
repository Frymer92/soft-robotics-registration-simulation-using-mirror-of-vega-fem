//
// Created by NewUser on 2019-02-28.
//

#ifndef TARGETUTILITIES_H
#define TARGETUTILITIES_H

#if defined(WIN32) || defined(_WIN32)
#include <windows.h>
#endif

#include "vec3d.h"
#include "vec2i.h"

using namespace std;

void targetCircularHorizontalCallBack(double * targetCenterPos,
                                      double r,
                                      double * targetPointPos,
                                      int axis1,
                                      int axis2);

void targetCircularHorizontalCallBack(Vec3d targetCenterPos,
                                      double r,
                                      Vec3d * targetPointPos,
                                      int axis1,
                                      int axis2);

double distance(Vec3d a, Vec3d b);
void norm(Vec3d a, Vec3d b, Vec3d * vnorm, double * dist);

void nearestNeighbor(vector<Vec3d> surfacePoints,
                     vector<Vec3d> pointCloud,
                     vector<Vec2i> * pointCorrespondence,
                     int * numPointCorrespondences,
                     double distance_threshold);

void closestPointsKDTree(vector<Vec3d> surfacePoints,
                         vector<Vec3d> pointCloud,
                         vector<Vec2i> * pointCorrespondence,
                         int * numPointCorrespondences,
                         double distance_threshold);

double MeanSquaredError(const vector<Vec3d> surfacePoints,
                        const vector<Vec3d> pointCloud,
                        const vector<Vec2i> pointCorrespondences);


#endif //TARGETUTILITIES_H
