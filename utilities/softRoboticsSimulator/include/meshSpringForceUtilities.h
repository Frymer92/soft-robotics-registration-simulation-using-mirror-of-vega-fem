//
// Created by NewUser on 2019-03-04.
//

#ifndef _MESHSPRINGFORCEUTILITIES_H
#define _MESHSPRINGFORCEUTILITIES_H

#include <iostream>
#include <stdlib.h>

using namespace std;

#include "vec3d.h"

#include "graph.h"
#include "tetMesh.h"

#include "getIntegratorSolver.h"
#include "volumetricMesh.h"

#include "implicitBackwardEulerSparse.h"
#include "eulerSparse.h"
#include "centralDifferencesSparse.h"

#include "corotationalLinearFEM.h"
#include "corotationalLinearFEMStencilForceModel.h"

#include "massSpringSystem.h"
#include "massSpringSystemFromObjMeshConfigFile.h"
#include "massSpringSystemFromTetMeshConfigFile.h"
#include "massSpringSystemFromCubicMeshConfigFile.h"
#include "massSpringStencilForceModel.h"

#include "forceModelAssembler.h"

//#include ""

// Extract and apply
void getSurfaceVertices(vector<Vec3d> * surfaceVertices,
                        IntegratorBase * integratorBase,
                        VolumetricMesh * volumetricMesh,
                        int n);

/* Manually applies a spring force to a point given a target.
 * target is the goal position
 * subject is the surface point in question
 * force is the force vector
 * forceFactor defines the magnitude of the force.
 */
void pointSpringForce(Vec3d target, Vec3d subject, Vec3d & forceVector,
                      double forceFactor, double deformableObjectCompliance);

// Applies spring forces to pre-defined point correspondences.
void pointToPointSpringForce (int numPointCorrespondences,
                              int * surfacePointIndices,
                              Vec3d * targetPointsPos,
                              IntegratorBase * integratorBase,
                              VolumetricMesh * volumetricMesh,
                              Graph * meshGraph1,
                              double * f_ext,
                              double forceFactor,
                              double deformableObjectCompliance,
                              int forceNeighborhoodSize);

#endif //_MESHSPRINGFORCEUTILITIES_H
