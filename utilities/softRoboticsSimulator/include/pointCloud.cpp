//
// Created by NewUser on 2019-03-04.
//

#include <fstream>
#include <sstream>
#include <random>
#include <vector>
#include "pointCloud.h"
#include "vec2i.h"
#include <matio.h>

using namespace std;


void PointCloud::AddPointCloud(const std::vector<Vec3d> pointBuffer, int numVertices) {
  pointClouds.push_back(pointBuffer);
  numPoints.push_back(numVertices);
  numPointClouds++;
}

void PointCloud::getPointCloud(std::vector<Vec3d> & pointBuffer, int pointCloudIndex) {
  if (pointCloudIndex < numPointClouds) {
    pointBuffer.resize(pointClouds[pointCloudIndex].size());
    for (int i = 0; i < pointClouds[pointCloudIndex].size(); i++) {
      pointBuffer[i] = pointClouds[pointCloudIndex][i];
    }
  }
}

int PointCloud::getNumPointClouds() const {
  return numPointClouds;
}

/*
 * IO MODULE FUNCTIONALITIES
 */

// READER WRITER MODULE
// Save data as ascii
int PointCloud::SaveToAscii(const char *filename) {
  ofstream meshFile (filename);
  if (meshFile.is_open()){
    meshFile << "#Custrom pointCloud storage\n";
    meshFile << "#Keyword for PointCloud seperators denoted as #*\n";
    char numPclouds[50];
    sprintf(numPclouds, "p %d\n", numPointClouds);
    meshFile << numPclouds;
    //meshFile << "#*\n";
    for (int i = 0; i < numPointClouds; i++) {
      char pcloudSize[50];
      sprintf(pcloudSize, "n %d\n", numPoints[i]);
      meshFile << pcloudSize;
      for (int j = 0; j < numPoints[i]; j++) {
        char buffer[50];
        Vec3d point = pointClouds[i][j];
        sprintf(buffer, "v %f %f %f\n", point[0], point[1], point[2]);
        meshFile << (string) buffer;
      }
      if (i <= numPointClouds) {
        meshFile << "#*\n";
      }
    }
    meshFile.close();
  }
  return 0;
}

void PointCloud::LoadFromAscii(const char *filename) {
  ifstream meshFile (filename);
  string line;
  if (meshFile.is_open()) {
    vector<Vec3d> tmpVec;
    int n;
    while (getline(meshFile, line)) {
      stringstream ss(line);

      char ident; ss >> ident;
      switch (ident) {
        case 'n': {
          char _;
          ss >> _;
          ss >> n;
          break;
        };
        case '#': {
          char s;
          ss >> s;
          if (s == '*') {
            AddPointCloud(tmpVec, n);
            numPoints.push_back(tmpVec.size());
            tmpVec.clear();
          }
          break;
        };
        case 'v': {
          double x;
          ss >> x;
          double y;
          ss >> y;
          double z;
          ss >> z;
          tmpVec.push_back(Vec3d(x, y, z));
          break;
        }
      }
    }
    pointClouds.push_back(tmpVec);
    numPoints.push_back(tmpVec.size());
  }
  meshFile.close();
}

/*
void PointCloud::ReadFromMat(const char *filename) {
  mat_t *matfp;
  matfp = Mat_Open(filename,MAT_ACC_RDONLY);
  Mat_Close(matfp);
}*/

/*
 * Point Cloud adjustments for synthetic data generaetion
 */

void PointCloud::addGaussianNoise(const int pointCloudIndex,
                                  const double mean,
                                  const double range,
                                  const double sample) {
  default_random_engine generator;
  normal_distribution<double> dist(mean, range);

  uniform_int_distribution<int> point_sample(0, pointClouds[pointCloudIndex].size());
  int new_size = int(float(pointClouds[pointCloudIndex].size()) * sample);
  vector<Vec3d> pcloud;
  for (int i = 0; i < new_size; i++) {
    int j = point_sample(generator);
    Vec3d vec = pointClouds[pointCloudIndex][j];
    Vec3d v = Vec3d(vec[0], vec[1],vec[2]);
    pcloud.push_back(v);
  }
  pointClouds[pointCloudIndex].clear();
  pointClouds[pointCloudIndex] = pcloud;

  for (auto& vec : pointClouds[pointCloudIndex]) {
    vec[0] = vec[0] + dist(generator);
    vec[1] = vec[1] + dist(generator);
    vec[2] = vec[2] + dist(generator);
  }

  pcloud.clear();
  for (int i = 0; i < pointClouds[pointCloudIndex].size(); i++){
    Vec3d a = pointClouds[pointCloudIndex][i];
    if ((abs(a[0]) < 1000 and
         abs(a[0]) < 1000 and
         abs(a[0]) < 1000)
      ) {
      pcloud.push_back(a);
    }
  }
  pointClouds[pointCloudIndex].clear();
  pointClouds[pointCloudIndex] = pcloud;

}

// Compute angle between plane vector and point.
double angleBetweenVectors(Vec3d a, Vec3d b) {
  double scalar = a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
  double magnitudeA = sqrt(pow(a[0],2) + pow(a[1],2) + pow(a[2],2));
  double magnitudeB = sqrt(pow(b[0],2) + pow(b[1],2) + pow(b[2],2));
  return acos(scalar/(magnitudeA*magnitudeB));
}

// Sever point cloud, by removing all points that are below a plane.
// Where plane is defined by the center and the orientation of the plane.
// The orientation is considered to be the upside surface.
void PointCloud::severPointClouds(const Vec3d center, const Vec3d orient) {
  double angle;
  for (int i = 0; i < numPointClouds; i++) {
    vector<Vec3d> newPcloud;
    for (int j = 0; j < pointClouds[i].size(); j++) {
      angle = angleBetweenVectors(orient, pointClouds[i][j] - center) * 180 / M_PI;
      if (angle < 90.0) {
        newPcloud.push_back(pointClouds[i][j]);
      }
    }
    pointClouds[i] = newPcloud;
  }
}
