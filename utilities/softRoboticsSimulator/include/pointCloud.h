//
// Created by NewUser on 2019-03-04.
//

#ifndef VEGAFEM_V4_0_POINTCLOUD_H
#define VEGAFEM_V4_0_POINTCLOUD_H

#include <stdlib.h>
#include <iostream>
#include <vector>

using namespace std;

#include "vec3d.h"
#include <matio.h>

class PointCloud
{
public:
    PointCloud(int numPointCloudsInitial = 0) {
      numPointClouds = numPointCloudsInitial;
    };

    void AddPointCloud(const std::vector<Vec3d> pointBuffer, int numVertices);
    void getPointCloud(std::vector<Vec3d> & pointBuffer, int pointCloudIndex);
    int getNumPointClouds() const;

    void addGaussianNoise(const int pointCloudIndex,
                          const double mean,
                          const double range,
                          const double sample);

    void severPointClouds(const Vec3d center,
                          const Vec3d orient);

    // .obj writer for series of point clouds
    int SaveToAscii(const char * filename);
    // .obj reader for point clouds
    void LoadFromAscii(const char * filename);

    // .mat reader for series of point clouds
    void ReadFromMat(const char * filename);

    protected:
        std::vector<std::vector<Vec3d>> pointClouds;
        int numPointClouds;
        std::vector<int> numPoints;
};


#endif //VEGAFEM_V4_0_POINTCLOUD_H
