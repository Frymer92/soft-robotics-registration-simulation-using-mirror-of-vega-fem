//
// Created by NewUser on 2019-05-23.
//

#include "displacementModel.h"
#include "meshSpringForceUtilities.h"
#include "targetUtilities.h"


void applyDisplacement(IntegratorBase * integratorBase,
                       VolumetricMesh * volumetricMesh,
                       vector<Vec2i> pointCorrespondences,
                       vector<Vec3d> uprime,
                       vector<Vec3d> experimentDeformation,
                       Graph * meshGraph1,
                       int forceNeighborhoodSize){
  int n = uprime.size();
  double * displacements = (double *) malloc (sizeof(double) * 3*n);
  integratorBase->GetqState(displacements);
  vector<Vec3d> surfaceVertices(volumetricMesh->getNumVertices());
  //double * surfaceVertices = (double *) malloc (sizeof(double) * 3 * n);
  volumetricMesh->exportMeshGeometry(surfaceVertices);
  Vec3d direction;
  double dist;
  double distanceFactor = 1.0/10.0;
  for (int i = 0; i < pointCorrespondences.size(); i++) {
    int surface_idx = pointCorrespondences[i][0];
    int pcloud_idx = pointCorrespondences[i][1];
    Vec3d position = uprime[surface_idx];
    Vec3d goal = experimentDeformation[pcloud_idx];
    norm(goal, position, &direction, &dist);
    uprime[surface_idx][0] = uprime[surface_idx][0] + direction[0]*dist*distanceFactor;
    uprime[surface_idx][1] = uprime[surface_idx][1] + direction[1]*dist*distanceFactor;
    uprime[surface_idx][2] = uprime[surface_idx][2] + direction[2]*dist*distanceFactor;
  }

  for (int i = 0; i < 3*n; i++){
    displacements[i] = uprime[i/3][i%3] - surfaceVertices[i/3][i%3];
  }

  integratorBase->SetqState(displacements);

}