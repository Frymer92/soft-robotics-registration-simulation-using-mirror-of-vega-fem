//
// Created by NewUser on 2019-09-10.
//

#include "experiment.h"
#include <fstream>
#include <sstream>


void Experiment::addDisplacement(float disp){
  displacement.push_back(disp);
}
void Experiment::addLoss(float l){
  loss.push_back(l);
}
void Experiment::addTime(int t){
  times.push_back(t);
}

void Experiment::addRealTime(double t){
  realTime.push_back(t);
}

void Experiment::addTipPos(Vec3d tPos){
  tipPos.push_back(tPos);
}

int Experiment::timestep() {
  return times.size();
}


void Experiment::experimentWriteToCSV (const char * filename){
  ofstream datafile (filename);
  if (datafile.is_open()){
    datafile << "timestep,realtime,x-tip,y-tip,z-tip,loss,displacement\n";

    for (int i = 0; i < times.size(); i++){
      char buffer[4096];
      sprintf(buffer, "%d,%f,%f,%f,%f,%f,%f\n",
      times[i], realTime[i], tipPos[i][0], tipPos[i][1], tipPos[i][2],
      loss[i], displacement[i]);
      datafile << buffer;
    }
    datafile.close();
  }
}