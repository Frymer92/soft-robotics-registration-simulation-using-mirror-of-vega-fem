//
// Created by NewUser on 2019-02-28.
//

#include "targetUtilities.h"

#include <ctime>
#include <cmath>
#include <algorithm>
#include <vector>
#include <iostream>
#include <limits>

using namespace std;

#include "KDTree.hpp"

//double max = std::numeric_limits<double>::max();
//double inf = std::numeric_limits<double>::infinity();


void targetCircularHorizontalCallBack (double * targetCenterPos,
                                       double r,
                                       double * targetPointPos,
                                       int axis1,
                                       int axis2) {
  double thresh = CLOCKS_PER_SEC*3;
  double theta = clock()/thresh;
  targetPointPos[axis1] = targetCenterPos[axis1] + r * cos(theta);
  targetPointPos[axis2] = targetCenterPos[axis2] + r * sin(theta);
  int axis3 = axis1 + axis2;
  switch (axis3) {
    case 3:
      axis3 = 0;
      break;
    case 2:
      axis3 = 1;
      break;
    case 1:
      axis3 = 2;
      break;
  }
  targetPointPos[axis3] = targetCenterPos[axis3];
}

// Computes circular coordinates for moving target,
void targetCircularHorizontalCallBack (Vec3d targetCenterPos,
                                       double r,
                                       Vec3d * targetPointPos,
                                       int axis1,
                                       int axis2) {
  double thresh = CLOCKS_PER_SEC*3;
  double theta = clock()/thresh;
  (*targetPointPos)[axis1] = targetCenterPos[axis1] + r * cos(theta);
  (*targetPointPos)[axis2] = targetCenterPos[axis2] + r * sin(theta);
  int axis3 = axis1 + axis2;
  switch (axis3) {
    case 3:
      axis3 = 0;
      break;
    case 2:
      axis3 = 1;
      break;
    case 1:
      axis3 = 2;
      break;
  }
  (*targetPointPos)[axis3] = targetCenterPos[axis3];
}

double distanceCalculate(double x1, double y1, double x2, double y2)
{
  double x = x1 - x2; //calculating number to square in next step
  double y = y1 - y2;
  double dist;

  dist = pow(x, 2) + pow(y, 2);       //calculating Euclidean distance
  dist = sqrt(dist);

  return dist;
}

double distance(Vec3d a, Vec3d b){
  double d = pow(a[0]-b[0], 2) + pow(a[1]-b[1],2) + pow(a[2] - b[2], 2);
  /*
  printf("%f,%f,%f\n", a[0], a[1], a[2]);
  printf("%f,%f,%f\n", b[0], b[1], b[2]);
  printf("%f\n", d);
  printf("%f\n", sqrt(d));
  */
  return sqrt(d);

}
// Provides vector direction and length.
void norm(Vec3d a, Vec3d b, Vec3d * vnorm, double * dist) {
  (*vnorm)[0] = a[0] - b[0];
  (*vnorm)[1] = a[1] - b[1];
  (*vnorm)[2] = a[2] - b[2];
  (*dist) = sqrt(pow((*vnorm)[0], 2) + pow((*vnorm)[1],2) + pow((*vnorm)[2], 2));
  (*vnorm)[0] = (*vnorm)[0]/(*dist);
  (*vnorm)[1] = (*vnorm)[1]/(*dist);
  (*vnorm)[2] = (*vnorm)[2]/(*dist);
}

// 1-NearestNeighbor search for point cloud and surface point correspondences.
// Naive solution for a mesh surface search.
void nearestNeighbor(vector<Vec3d> surfacePoints,
                     vector<Vec3d> pointCloud,
                     vector<Vec2i> * pointCorrespondences,
                     int * numPointCorrespondences,
                     double distance_threshold) {
  double inf = std::numeric_limits<double>::infinity();

  // Initial distancefield
  double tmpDist;

  //(*pointCorrespondences).resize(0);
  for (int i = 0; i < pointCloud.size(); i++) {
    double bestDist = inf;
    Vec2i best;
    for (int j = 0; j < surfacePoints.size(); j++) {
      tmpDist = distance(pointCloud[i], surfacePoints[j]);
      if (tmpDist < bestDist) {
        best = Vec2i(i, j);
        bestDist = tmpDist;
      }
      (*pointCorrespondences).push_back(best);
    }
  }
  *numPointCorrespondences = (*pointCorrespondences).size();
}

void closestPointsKDTree(vector<Vec3d> surfacePoints,
                         vector<Vec3d> pointCloud,
                         vector<Vec2i> * pointCorrespondence,
                         int * numPointCorrespondences,
                         double distance_threshold) {
  pointVec points;
  point_t pt;
  Vec3d ptc;

  for (int i = 0; i < surfacePoints.size(); i++) {
    ptc = surfacePoints[i];
    pt = {ptc[0], ptc[1], ptc[2]};
    points.push_back(pt);
  }
  KDTree tree(points);

  if (surfacePoints.size() == 0) {
    *numPointCorrespondences = 0;
    return;
  }
  (*pointCorrespondence).resize(0);
  //*numPointCorrespondences = pointCloud.size();
  for (int j = 0; j < pointCloud.size(); j++) {
    ptc = pointCloud[j];
    pt = {ptc[0], ptc[1], ptc[2]};
    int id = tree.nearest_index(pt);
    if (distance(ptc, surfacePoints[id]) < distance_threshold) {
      //(*pointCorrespondence)[j] = Vec2i(id, j);
      (*pointCorrespondence).push_back(Vec2i(id, j));
    }
  }
  *numPointCorrespondences = (*pointCorrespondence).size();
}

double MeanSquaredError (const vector<Vec3d> surfacePoints,
                         const vector<Vec3d> pointCloud,
                         const vector<Vec2i> pointCorrespondences) {
  double mse = 0.0;
  float tmp;
  int count = 0;
  for (auto &&v : pointCorrespondences) {

    tmp = distance(pointCloud[v[1]], surfacePoints[v[0]]);

    if (tmp > 1000) {
      Vec3d a = pointCloud[v[1]];
      Vec3d b = surfacePoints[v[0]];
      printf("pcloud : %d,%f,%f,%f\nsurface : %d,%f,%f,%f\n",
        v[1], a[0], a[1], a[2],
             v[0],b[0],b[1],b[2]);

      double a1 = pow(a[0]-b[0], 2);
      double a2 = pow(a[1]-b[1],2);
      double a3 = pow(a[2]-b[2], 2);
      printf("x : %f, y : %f, z : %f\n", a1, a2, a3);
      double a4 = a1 + a2 + a3;
      printf("squared : %f, square root : %f\n", a4, sqrt(a4));
      //mse = mse + tmp;
      //count++;
      //char s = getchar();
    }
    else
    if (!std::isnan(tmp)) {
      mse = mse + tmp;
      count++;
    } else {
      printf("%d,%f,%f,%f\n%d,%f,%f,%f\n", v[0],
             pointCloud[v[0]][0],
             pointCloud[v[0]][1],
             pointCloud[v[0]][2],
             v[1],
             surfacePoints[v[1]][0],
             surfacePoints[v[1]][1],
             surfacePoints[v[1]][2]);
      Vec3d a = surfacePoints[v[0]];
      Vec3d b = surfacePoints[v[1]];
      double a1 = pow(a[0]-b[0], 2);
      double a2 = pow(a[1]-b[1],2);
      double a3 = pow(a[2]-b[2], 2);

      printf("x : %f, y : %f, z : %f\n", a1, a2, a3);

      double a4 = a1 + a2 + a3;
      printf("squared : %f, square root : %f\n", a4, sqrt(a4));
  }
}
if (count > 0) {
  return mse / count;
} else {
  return 1e300;
}

}