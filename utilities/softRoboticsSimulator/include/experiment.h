//
// Created by NewUser on 2019-09-10.
//

#ifndef THESISCODE_EXPERIMENT_H
#define THESISCODE_EXPERIMENT_H

#include <vector>
#include <iostream>
#include <stdlib.h>

using namespace std;

#include "vec3d.h"

class Experiment {
private:
    vector<float> displacement;
    vector<float> loss;
    vector<int> times;
    vector<Vec3d> tipPos;
    vector<double> realTime;


public:
    void addDisplacement(float disp);
    void addLoss(float l);
    void addTime(int t);
    void addTipPos(Vec3d tPos);
    void addRealTime(double t);
    int timestep();

    void experimentWriteToCSV (const char * filename);
};




#endif //THESISCODE_EXPERIMENT_H
