//
// Created by NewUser on 2019-05-23.
//

#ifndef THESISCODE_DISPLACEMENTMODEL_H
#define THESISCODE_DISPLACEMENTMODEL_H

#include <iostream>
#include <stdlib.h>

using namespace std;

#include "vec3d.h"
#include "vec2i.h"

#include "graph.h"
#include "tetMesh.h"

#include "getIntegratorSolver.h"
#include "volumetricMesh.h"

#include "implicitBackwardEulerSparse.h"
#include "eulerSparse.h"
#include "centralDifferencesSparse.h"

#include "corotationalLinearFEM.h"
#include "corotationalLinearFEMStencilForceModel.h"

#include "massSpringSystem.h"
#include "massSpringSystemFromObjMeshConfigFile.h"
#include "massSpringSystemFromTetMeshConfigFile.h"
#include "massSpringSystemFromCubicMeshConfigFile.h"
#include "massSpringStencilForceModel.h"

#include "forceModelAssembler.h"

void applyDisplacement(IntegratorBase * integratorBase,
                       VolumetricMesh * volumetricMesh,
                       vector<Vec2i> pointCorrespondences,
                       vector<Vec3d> uprime,
                       vector<Vec3d> experimentDeformation,
                       Graph * meshGraph1,
                       int forceNeighborhoodSize);

#endif //THESISCODE_DISPLACEMENTMODEL_H
