//
// Created by NewUser on 2019-03-04.
//

#include "meshSpringForceUtilities.h"

#include <vector>
#include <iostream>

//using namespace std;

// Manually applies a spring force to a point given a target.
// target is the goal position
// subject is the surface point in question
// force is the force vector
// forceFactor defines the magnitude of the force.

void pointSpringForce(Vec3d target, Vec3d subject, Vec3d & forceVector,
                      double forceFactor, double deformableObjectCompliance) {
  // Directional vector
  double dirX = (subject[0] - target[0]);
  double dirY = (subject[1] - target[1]);
  double dirZ = (subject[2] - target[2]);

  // force magnitude
  double dist = sqrt(pow(dirX, 2) + pow(dirY, 2) + pow(dirZ, 2));
  if (dist <= 0.0) {
    forceVector = Vec3d(0,0,0);
    return;
  }
  double force = dist * (-forceFactor) * deformableObjectCompliance;
  // Directional force
  forceVector[0] = dirX/dist * force;
  forceVector[1] = dirY/dist * force;
  forceVector[2] = dirZ/dist * force;
}

void getSurfaceVertices(std::vector<Vec3d> * surfaceVertices,
                        IntegratorBase * integratorBase,
                        VolumetricMesh * volumetricMesh,
                        int n) {
  double * uprime = (double *) malloc (sizeof(double) * 3*n);
  integratorBase->GetqState(uprime);

  (*surfaceVertices).resize(n);
  volumetricMesh->exportMeshGeometry(*surfaceVertices);

  for (int i = 0; i < n ; i++) {
    (*surfaceVertices)[i] = Vec3d(uprime[3 * i] + (*surfaceVertices)[i][0],
                                  uprime[3 * i + 1] + (*surfaceVertices)[i][1],
                                  uprime[3 * i + 2] + (*surfaceVertices)[i][2]);
  }
}


void pointToPointSpringForce (int numPointCorrespondences,
                              int * surfacePointIndices,
                              Vec3d * targetPointsPos,
                              IntegratorBase * integratorBase,
                              VolumetricMesh * volumetricMesh,
                              Graph * meshGraph1,
                              double * f_ext,
                              double forceFactor,
                              double deformableObjectCompliance,
                              int forceNeighborhoodSize) {
  // locate current point positions, for computing spring force.
  vector <Vec3d> externalForce(numPointCorrespondences);
  int n = volumetricMesh->getNumVertices();
  vector <Vec3d> uprime(n);

  //getSurfaceVertices(&uprime);
  getSurfaceVertices(&uprime, integratorBase, volumetricMesh, n);
  /*
  printf("current pos %f %f %f\n", uprime[sp][0], uprime[sp][1], uprime[sp][2]);
  printf("getting point cloud %f %f %f\n", targetPointsPos[sp][0], targetPointsPos[sp][1], targetPointsPos[sp][2]);
  printf("correspondences: %d %lu\n", numPointCorrespondences, uprime.size());
  */

  for (int i = 0; i < numPointCorrespondences; i++) {
    //Vec3d goalPointPos = targetPointsPos[i];//Vec3d(targetPointsPos[3 * i], targetPointsPos[3 * i + 1], targetPointsPos[3 * i + 2]);
    pointSpringForce(targetPointsPos[i], uprime[surfacePointIndices[i]], (externalForce[i]), forceFactor,
                     deformableObjectCompliance);

    //pointSpringForce(targetPointsPos[i], uprime, surfacePointIndices[i], externalForce[i]);
    f_ext[3 * surfacePointIndices[i] + 0] += externalForce[i][0];
    f_ext[3 * surfacePointIndices[i] + 1] += externalForce[i][1];
    f_ext[3 * surfacePointIndices[i] + 2] += externalForce[i][2];

    // register force on the pulled vertex
    // distribute force over the neighboring vertices
    set<int> affectedVertices;
    set<int> lastLayerVertices;

    affectedVertices.insert(surfacePointIndices[i]);
    lastLayerVertices.insert(surfacePointIndices[i]);
    //printf("%d fx: %G fy: %G fz: %G | %G %G %G\n", surfaceVertex, forceX, forceY, forceZ, externalForce[0], externalForce[1], externalForce[2]);

    for (int j = 1; j < forceNeighborhoodSize; j++) {
      // linear kernel
      double forceMagnitude = 1.0 * (forceNeighborhoodSize - j) / forceNeighborhoodSize;

      set<int> newAffectedVertices;
      for (set<int>::iterator iter = lastLayerVertices.begin(); iter != lastLayerVertices.end(); iter++) {
        // traverse all neighbors and check if they were already previously inserted
        int vtx = *iter;
        int deg = meshGraph1->GetNumNeighbors(vtx);
        for (int k = 0; k < deg; k++) {
          int vtxNeighbor = meshGraph1->GetNeighbor(vtx, k);

          if (affectedVertices.find(vtxNeighbor) == affectedVertices.end()) {
            // discovered new vertex
            newAffectedVertices.insert(vtxNeighbor);
          }
        }
      }

      lastLayerVertices.clear();
      for (set<int>::iterator iter = newAffectedVertices.begin(); iter != newAffectedVertices.end(); iter++) {
        // apply force
        f_ext[3 * *iter + 0] += forceMagnitude * externalForce[i][0];
        f_ext[3 * *iter + 1] += forceMagnitude * externalForce[i][1];
        f_ext[3 * *iter + 2] += forceMagnitude * externalForce[i][2];
        // generate new layers
        lastLayerVertices.insert(*iter);
        affectedVertices.insert(*iter);
      }
    }
  }
}
