KEYBOARD SHORTCUTS:

esc - exits the simulation.
w - render wireframe
c - render point correspondences
C - render point cloud
s - render surface mesh
i - get camera orientation
\ - reset camera
B - list 10 first points of mesh, for debugging purposes.
V - render vertices
a - render axes
b - render fixed vertices
n - proceed to next timestep goal
l - pause and lock simulation to freely move.
e - render surface
E - render secondary (painted surface)
z - 