/*************************************************************************
 *                                                                       *
 * Vega FEM Simulation Library Version 4.0                               *
 *                                                                       *
 * "Interactive deformable object simulator" driver application,         *
 *  Copyright (C) 2007 CMU, 2009 MIT, 2018 USC                           *
 *                                                                       *
 * All rights reserved.                                                  *
 *                                                                       *
 * Code authors: Jernej Barbic, Fun Shing Sin, Daniel Schroeder          *
 * http://www.jernejbarbic.com/vega                                      *
 *                                                                       *
 * Research: Jernej Barbic, Hongyi Xu, Yijing Li,                        *
 *           Danyong Zhao, Bohan Wang,                                   *
 *           Fun Shing Sin, Daniel Schroeder,                            *
 *           Doug L. James, Jovan Popovic                                *
 *                                                                       *
 * Funding: National Science Foundation, Link Foundation,                *
 *          Singapore-MIT GAMBIT Game Lab,                               *
 *          Zumberge Research and Innovation Fund at USC,                *
 *          Sloan Foundation, Okawa Foundation,                          *
 *          USC Annenberg Foundation                                     *
 *                                                                       *
 * This utility is free software; you can redistribute it and/or         *
 * modify it under the terms of the BSD-style license that is            *
 * included with this library in the file LICENSE.txt                    *
 *                                                                       *
 * This utility is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the file     *
 * LICENSE.TXT for more details.                                         *
 *                                                                       *
 *************************************************************************/

/*****************************************************************************

Interactive deformable object simulator (3D nonlinear solid deformable objects).

Supported integrators:
- implicit backward Euler
- implicit Newmark
- central differences
- Euler
- symplectic Euler

Supported meshes:
- tetrahedral
- cubic

Supported materials:
- linear
- rotated linear (corotational linear FEM)
- Saint-Venant Kirchoff
- invertible Saint-Venant Kirchoff
- invertible neo-Hookean 
- invertible Mooney-Rivlin
- mass-spring system

*******************************************************************************/

#include <stdlib.h>
#include <iostream>
#include <vector>
#include <string>
#include <cstdio>
#include <cassert>
#include <float.h>
#include <math.h>
#include <ctime>
#include <limits>
#include <fstream>
#include <opencv2/opencv.hpp>
#include <chrono>

using namespace std;

#if defined(WIN32) || defined(_WIN32)
  #include <windows.h>
#endif

#ifdef __APPLE__
  #include "TargetConditionals.h"
#endif

// Custom utilities for Thesis API.
#include "targetUtilities.h"
#include "meshSpringForceUtilities.h"
#include "pointCloud.h"
#include "KDTree.hpp"
#include "displacementModel.h"
#include "experiment.h"

#include "getopts.h"
#include "initGraphics.h"
#include "sceneObjectDeformable.h"
#include "performanceCounter.h"

#include "volumetricMeshLoader.h"
#include "tetMesh.h"

#include "StVKElementABCDLoader.h"
#include "StVKCubeABCD.h"
#include "StVKTetABCD.h"
#include "StVKTetHighMemoryABCD.h"
#include "StVKFEM.h"
#include "StVKStencilForceModel.h"

#include "implicitBackwardEulerSparse.h"
#include "eulerSparse.h"
#include "centralDifferencesSparse.h"

#include "corotationalLinearFEM.h"
#include "corotationalLinearFEMStencilForceModel.h"

#include "isotropicHyperelasticFEM.h"
#include "isotropicMaterial.h"
#include "StVKIsotropicMaterial.h"
#include "neoHookeanIsotropicMaterial.h"
#include "MooneyRivlinIsotropicMaterial.h"
#include "isotropicHyperelasticFEMStencilForceModel.h"

#include "getIntegratorSolver.h"
#include "generateMeshGraph.h"
#include "generateMassMatrix.h"

#include "massSpringSystem.h"
#include "massSpringSystemFromObjMeshConfigFile.h"
#include "massSpringSystemFromTetMeshConfigFile.h"
#include "massSpringSystemFromCubicMeshConfigFile.h"
#include "massSpringStencilForceModel.h"

#include "linearFEMStencilForceModel.h"

#include "forceModelAssembler.h"

#include "graph.h"
#include "renderSprings.h"
#include "configFile.h"

#include "lighting.h"
#include "listIO.h"
#include "matrixIO.h"
#include "averagingBuffer.h"

#include "GL/glui.h"

// graphics 
char windowTitleBase[4096] = "Real-time sim";
void displayFunction(void);
int windowID;
int windowWidth = 800;
int windowHeight = 600;
double zNear=0.01;
double zFar=10.0;
double cameraRadius;
double focusPositionX, focusPositionY, focusPositionZ;
double cameraLongitude, cameraLattitude;
SphericalCamera * camera = nullptr;
int g_iLeftMouseButton=0, g_iMiddleMouseButton=0, g_iRightMouseButton=0;
int g_vMousePos[2] = {0,0};
int shiftPressed=0;
int altPressed=0;
int ctrlPressed=0;
int renderWireframe=0;
int renderAxes=0;
int renderDeformableObject=1;
int renderSecondaryDeformableObject=1;
int useRealTimeNormals = 0;
int renderGroundPlane = 0;
int renderFixedVertices = 1;
int renderSprings = 0;
int renderVertices = 0;
int lockScene=0;
int pauseSimulation=0;
int singleStepMode=0;
Lighting * lighting = nullptr;
SceneObjectDeformable * deformableObjectRenderingMesh = nullptr;
SceneObjectDeformable * secondaryDeformableObjectRenderingMesh = nullptr;
SceneObject * extraSceneGeometry = nullptr;
char groundPlaneString[128];
double groundPlaneHeight;
double groundPlaneLightHeight = 10.0;
double groundPlaneSize = 15.0;
GLuint displayListGround;

// config file
string configFilename;
char renderingMeshFilename[4096];
char secondaryRenderingMeshFilename[4096];
char secondaryRenderingMeshInterpolationFilename[4096];
char volumetricMeshFilename[4096];
char customMassSpringSystem[4096];
char deformableObjectMethod[4096];
char fixedVerticesFilename[4096];
char massSpringSystemObjConfigFilename[4096];
char massSpringSystemTetMeshConfigFilename[4096];
char massSpringSystemCubicMeshConfigFilename[4096];
char invertibleMaterialString[4096] = "__none";
char initialPositionFilename[4096];
char initialVelocityFilename[4096];
char forceLoadsFilename[4096];
char outputFilename[4096];
int corotationalLinearFEM_warp = 1;
const int max_corotationalLinearFEM_warp = 2;
char implicitSolverMethod[4096];
char solverMethod[4096];
char extraSceneGeometryFilename[4096];
char lightingConfigFilename[4096];
float dampingMassCoef; // Rayleigh mass damping
float dampingStiffnessCoef; // Rayleigh stiffness damping
float dampingLaplacianCoef = 0.0; // Laplacian damping (rarely used)
float deformableObjectCompliance = 1.0; // scales all user forces by the provided factor

// adjusts the stiffness of the object to cause all frequencies scale by the provided factor:
// keep it to 1.0 (except for experts)
float frequencyScaling = 1.0; 
int maxIterations; // for implicit integration
double epsilon; // for implicit integration
char backgroundColorString[4096] = "255 255 255";
int numInternalForceThreads;
int numSolverThreads;

// simulation
int syncTimestepWithGraphics=1;
float timeStep = 1.0 / 30;
float newmarkBeta = 0.25;
float newmarkGamma = 0.5;
int use1DNewmarkParameterFamily = 1;
int substepsPerTimeStep = 1;
double inversionThreshold;
double fps = 0.0;
AveragingBuffer fpsBuffer(5);
double cpuLoad = 0;
double forceAssemblyTime = 0.0;
AveragingBuffer forceAssemblyBuffer(50);
double systemSolveTime = 0.0;
AveragingBuffer systemSolveBuffer(50);
int enableTextures = 0;
int staticSolver = 0;
int graphicFrame = 0;
int lockAt30Hz = 0;
int pulledVertex = -1;
int forceNeighborhoodSize = 1;
int dragStartX, dragStartY;
int explosionFlag = 0;
PerformanceCounter titleBarCounter;
PerformanceCounter explosionCounter;
PerformanceCounter cpuLoadCounter;
int timestepCounter = 0;
int subTimestepCounter = 0;
int numFixedVertices;
int * fixedVertices;
int numForceLoads = 0;
double * forceLoads = nullptr;
IntegratorBase * integratorBase = nullptr;
ImplicitNewmarkSparse * implicitNewmarkSparse = nullptr;
IntegratorBaseSparse * integratorBaseSparse = nullptr;
ForceModel * forceModel = nullptr;
int enableCompressionResistance = 1;
double compressionResistance = 500;
int centralDifferencesTangentialDampingUpdateMode = 1;
int addGravity=0;
double g=9.81;

// Custom Variables for running time based deformation based experiments
clock_t startTime;
double inf = std::numeric_limits<double>::infinity();
double elapsedTime= inf;
double forceFactor = 1.0;
//
char experimentFilename[4096];
char experimentType[4096];
char registrationModel[4096];
PointCloud experimentPointClouds;
double noise_mean = 0.0;
double noise_range = 0.0;
double sample_size = 1.0;

Vec3d partialPointCloudCenter;
Vec3d partialPointCloudOrient;

bool fixedTimestepGoal = true;
int goalDeformationTimeStep = 0;
int goaldeformationstride = 1;
char automatedExperiment[4096];

// Custom plot variables
double pointCorrespondencePlotLineWidt = 1.2;
int renderPointCorrespondences = 0;
int renderPointCloud = 1;
int renderSurfaceMesh = 1;

// Custom target functionality pre-defined variable
/*
 * Target function can be any predefined functionality
 * "circular"
 *
 */
char targetFunction[4096] = "no target";
Vec3d targetCenterPos1 = Vec3d(-inf, -inf, -inf);
Vec3d targetPointPos1 = Vec3d(inf, inf, inf);
int surfaceTargetPoint = -1;
double targetPlotSize = 1.0;
double r = 4.0;
int axis1 = 0;
int axis2 = 1;
vector <Vec2i> pointCorrespondences;
vector <Vec3d> last_uprime;
vector <Vec2i> last_correspondences;

Experiment * expData = new Experiment;
vector<float> displacement;
vector<float> loss;
vector<int> times;
int tipPosIndex = 0;
vector<Vec3d> tipPos;
char experimentDataFileName[4096];

double epsilonMSE = -inf;
double epsilonMSEmin = inf;
double epsilonDisplacement = -inf;
double experimentPlotSize = 1.0;
double max_point_correspondence_distance = 20.0;
double min_point_correspondence_distance = 40.0;

chrono::time_point<chrono::system_clock> start;
bool timerSet = false;
int experimentReset = -1;

VolumetricMesh * volumetricMesh = nullptr;
TetMesh * tetMesh = nullptr;
Graph * meshGraph = nullptr;

StVKFEM * stVKFEM = nullptr;

enum massSpringSystemSourceType { OBJ, TETMESH, CUBICMESH, CHAIN, NONE } massSpringSystemSource = NONE;
enum deformableObjectType { STVK, COROTLINFEM, LINFEM, MASSSPRING, INVERTIBLEFEM, UNSPECIFIED } deformableObject = UNSPECIFIED;
enum invertibleMaterialType { INV_STVK, INV_NEOHOOKEAN, INV_MOONEYRIVLIN, INV_NONE } invertibleMaterial = INV_NONE;
enum solverType { IMPLICITNEWMARK, IMPLICITBACKWARDEULER, EULER, SYMPLECTICEULER, CENTRALDIFFERENCES, UNKNOWN } solver = UNKNOWN;

StencilForceModel * stencilForceModel = nullptr;
ForceModelAssembler *forceModelAssembler = nullptr;
CorotationalLinearFEMStencilForceModel * corotationalLinearFEMStencilForceModel = nullptr;
IsotropicHyperelasticFEMStencilForceModel * isotropicHyperelasticFEMStencilForceModel = nullptr;
MassSpringStencilForceModel * massSpringStencilForceModel = nullptr;
StVKStencilForceModel * stVKStencilForceModel = nullptr;
LinearFEMStencilForceModel * linearFEMStencilForceModel = nullptr;

MassSpringSystem * massSpringSystem = nullptr;
RenderSprings * renderMassSprings = nullptr;
SparseMatrix * massMatrix = nullptr;
SparseMatrix * LaplacianDampingMatrix = nullptr;

int n;
double * u = nullptr;
double * uvel = nullptr;
double * uaccel = nullptr;
double * f_ext = nullptr;
double * f_extBase = nullptr;
double * uSecondary = nullptr;
double * uInitial = nullptr;
double * velInitial = nullptr;

// interpolation to secondary mesh
int secondaryDeformableObjectRenderingMesh_interpolation_numElementVertices;
int * secondaryDeformableObjectRenderingMesh_interpolation_vertices = nullptr;
double * secondaryDeformableObjectRenderingMesh_interpolation_weights = nullptr;

// glui
GLUI * glui;
GLUI_Spinner * timeStep_spinner;
GLUI_StaticText * systemSolveStaticText;
GLUI_StaticText * forceAssemblyStaticText;

void callAllUICallBacks();
void Sync_GLUI();
void stopDeformations_buttonCallBack(int code);

// Timer for experiment duration.
void testExperimentExpired() {
  float seconds_passed= ((double) clock() - startTime)/CLOCKS_PER_SEC;
  if (seconds_passed >= elapsedTime &&
      elapsedTime != inf) {
    printf("Terminated at time %f from simulation start.\n", seconds_passed);
    if ((std::strcmp(experimentType, "deformation") == 0)) {
      printf("filename written to : %s\n", experimentFilename);
      experimentPointClouds.SaveToAscii(experimentFilename);
    }
    /*fstream experimentFile (experimentFilename);
    if (experimentFile.is_open())
    {
      experimentFile.close();
    }*/
    exit(0);
  }
}

//font is, for example, GLUT_BITMAP_9_BY_15
void print_bitmap_string(float x, float y, float z, void * font, char * s)
{
  glRasterPos3f(x,y,z);
  if (s && strlen(s)) 
  {
    while (*s) 
    {
      glutBitmapCharacter(font, *s);
      s++;
    }
  }
}

// renders the ground plane
void RenderGroundPlane(double groundPlaneHeight, double rPlane, double gPlane, double bPlane, double ambientFactor, double diffuseFactor, double specularFactor, double shininess)
{
  glEnable(GL_POLYGON_OFFSET_FILL);
  glPolygonOffset(1.0,1.0);

  float planeAmbient[4] = { (float)(ambientFactor*rPlane), (float)(ambientFactor*gPlane), (float)(ambientFactor*bPlane), 1.0};
  float planeDiffuse[4] = { (float)(diffuseFactor*rPlane), (float)(diffuseFactor*gPlane), (float)(diffuseFactor*bPlane), 1.0};
  float planeSpecular[4] = { (float)(specularFactor*rPlane), (float)(specularFactor*gPlane), (float)(specularFactor*bPlane), 1.0};
  float planeShininess = shininess; 
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, planeAmbient);
  glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, planeDiffuse);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, planeSpecular);
  glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, planeShininess);

  glNormal3f(0,1,0);
  const int planeResolution = 100;
  double planeIncrement = groundPlaneSize / planeResolution;
  for(int i=0; i<planeResolution; i++)
    for(int j=0; j<planeResolution; j++)
    {
      glBegin(GL_QUADS);
      glVertex3f(-groundPlaneSize/2 + i * planeIncrement, groundPlaneHeight, -groundPlaneSize/2 + j * planeIncrement);
      glVertex3f(-groundPlaneSize/2 + i * planeIncrement, groundPlaneHeight, -groundPlaneSize/2 + (j+1) * planeIncrement);
      glVertex3f(-groundPlaneSize/2 + (i+1) * planeIncrement, groundPlaneHeight, -groundPlaneSize/2 + (j+1) * planeIncrement);
      glVertex3f(-groundPlaneSize/2 + (i+1) * planeIncrement, groundPlaneHeight, -groundPlaneSize/2 + j * planeIncrement);
      glEnd();
    }
  glDisable(GL_POLYGON_OFFSET_FILL);
}

void plotPoint(double size, Vec3d plotPointPos, double * colorScheme) {
  // Target rendering
  glColor3f(colorScheme[0], colorScheme[1], colorScheme[2]);
  glEnable(GL_POLYGON_OFFSET_POINT);
  glPolygonOffset(-1.0,-1.0);
  glPointSize(size);
  glBegin(GL_POINTS);
  glVertex3f(plotPointPos[0], plotPointPos[1], plotPointPos[2]);
  glEnd();
  glDisable(GL_POLYGON_OFFSET_FILL);
}

void plotPoints(double size, int numPoints, vector<Vec3d> plotPointsPos, double * colorScheme) {
  for (int i = 0; i < numPoints; i++) {
    plotPoint(size, plotPointsPos[i], colorScheme);
  }
}

void plotPointCloudFromMesh(VolumetricMesh * mesh, double * targetPointColorScheme1) {
  // Fetches and plots initial state of the deformation, e.g. X
  std::vector<Vec3d> vPrime(mesh->getNumVertices());
  mesh->exportMeshGeometry(vPrime);

  //double targetPointColorScheme1[3] = {0.6, 0.9, 0.2};
  plotPoints(3.0, mesh->getNumVertices(), vPrime, targetPointColorScheme1);
}

void plotLine(double size, Vec3d startPoint, Vec3d endPoint, double * colorScheme) {
  glPushAttrib(GL_ENABLE_BIT);
  //# glPushAttrib is done to return everything to normal after drawing

  glLineStipple(1, 0xAAAA);
  glLineWidth(size);
  glColor3f(colorScheme[0], colorScheme[1], colorScheme[2]);
  glEnable(GL_LINE_STIPPLE);
  glBegin(GL_LINES);
  glVertex3f(startPoint[0], startPoint[1], startPoint[2]);
  glVertex3f(endPoint[0], endPoint[1], endPoint[2]);
  glEnd();

  glPopAttrib();
}

void saveExperiment(char * file_ext) {
  char fname[4096];
  sprintf(fname, "%s%s", experimentDataFileName, file_ext);
  printf("%s\n", fname);
  expData->experimentWriteToCSV(fname);
  //experimentWriteToCSV(experimentDataFileName, times, displacement, loss, tipPos);
}

void plotPointCorrespondences(vector<Vec3d> pCloud,
                              double size,
                              double * lineColorScheme) {
  vector<Vec3d> uprime(volumetricMesh->getNumVertices());
  getSurfaceVertices(&uprime, integratorBase, volumetricMesh, n);
  vector<double> dists;
  double longest = 0.0;
  for (int i = 0; i < pointCorrespondences.size(); i++) {
    double tmpdist = distance(pCloud[pointCorrespondences[i][1]],
                              uprime[pointCorrespondences[i][0]]);
    if (tmpdist > longest) {
      longest = tmpdist;
    }
    dists.push_back(tmpdist);
  }

  for (int i = 0; i < pointCorrespondences.size(); i++) {
    double ratio = dists[i]/longest;
    double scale[3] = {ratio, 1-ratio,0};
    plotLine(size,
             pCloud[pointCorrespondences[i][1]],
             uprime[pointCorrespondences[i][0]],
             scale);
  }
}

void plotPointCorrespondences(vector<Vec3d> pCloud,
                              double size,
                              double * lineColorScheme,
                              double longest) {
  vector<Vec3d> uprime(volumetricMesh->getNumVertices());
  getSurfaceVertices(&uprime, integratorBase, volumetricMesh, n);

  for (int i = 0; i < pointCorrespondences.size(); i++) {
    double ratio = distance(pCloud[pointCorrespondences[i][1]],
                            uprime[pointCorrespondences[i][0]])/longest;
    double scale[3] = {ratio, 1-ratio,0};
    plotLine(size,
             pCloud[pointCorrespondences[i][1]],
             uprime[pointCorrespondences[i][0]],
             scale);
  }
}

void resetExperiment(){

  goalDeformationTimeStep = goalDeformationTimeStep+goaldeformationstride;
  pauseSimulation = 0;
  timerSet = false;


  if (experimentReset == 0) {
    stopDeformations_buttonCallBack(0);
    pointCorrespondences.clear();
  }
  if (experimentReset == 1 and
      goalDeformationTimeStep >= experimentPointClouds.getNumPointClouds()) {
    printf("Terminating\n");
    exit(0);
  }
}
void write_frame() {
  /*glReadPixels(GLint x,
    GLint y,
    GLsizei width,
    GLsizei height,
    GLenum format,
    GLenum type,
    GLvoid * data);
    */

  cv::Mat a;
  //cv::waitKey(0);
  //exit(0);
}

// graphics loop function.
void displayFunction(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

  // setup model transformations
  glMatrixMode(GL_MODELVIEW); 
  glLoadIdentity();

  camera->Look();

  // set OpenGL lighting 
  deformableObjectRenderingMesh->SetLighting(lighting);

  glEnable(GL_LIGHTING);
  glPolygonOffset(0.0,0.0);
  glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

  glStencilFunc(GL_ALWAYS, 0, ~(0u));
  // render embedded triangle mesh
  if (renderSecondaryDeformableObject)
    secondaryDeformableObjectRenderingMesh->Render();

  glStencilFunc(GL_ALWAYS, 1, ~(0u));

  // render the main deformable object (surface of volumetric mesh)
  if (renderDeformableObject)
  {
    if (renderSecondaryDeformableObject)
    {
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      glEnable(GL_BLEND);

      glEnable(GL_POLYGON_OFFSET_FILL);
      glPolygonOffset(1.0, 1.0);
      glDrawBuffer(GL_NONE);
      deformableObjectRenderingMesh->Render();
      glDisable(GL_POLYGON_OFFSET_FILL);
      glDrawBuffer(GL_BACK);
      glEnable(GL_LIGHTING);
    }

    if (renderSurfaceMesh) {
      glColor3f(0.0, 0.0, 0.0);
      deformableObjectRenderingMesh->Render();
    }
    if (renderVertices)
    {
      glDisable(GL_LIGHTING);
      glColor3f(0.5,0,0);
      glPointSize(3.0);
      deformableObjectRenderingMesh->RenderVertices();
      glEnable(GL_LIGHTING);
    }

    if (renderSecondaryDeformableObject)
    {
      glDisable(GL_BLEND);
    }
  }

  // render any extra scene geometry
  glStencilFunc(GL_ALWAYS, 0, ~(0u));
  if (extraSceneGeometry != nullptr)
    extraSceneGeometry->Render();

  double ground[4] = {0,1,0,-groundPlaneHeight-0.01};
  double light[4] = {0,groundPlaneLightHeight,0,1}; 

  glDisable(GL_TEXTURE_2D);

  // render shadow
  if (renderGroundPlane)
  {
    glColor3f(0.1,0.1,0.1);
    glDisable(GL_LIGHTING);
    deformableObjectRenderingMesh->RenderShadow(ground, light);

    if (extraSceneGeometry != nullptr)
      extraSceneGeometry->RenderShadow(ground, light);
    glEnable(GL_LIGHTING);
    glCallList(displayListGround);
  }

  glDisable(GL_LIGHTING);

  glStencilFunc(GL_ALWAYS, 1, ~(0u));
  glColor3f(0,0,0);
  if (renderWireframe)
    deformableObjectRenderingMesh->RenderEdges();

  // disable stencil buffer modifications
  glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

  glColor3f(0,0,0);

  if (renderAxes)
  {
    glLineWidth(1.0);
    drawAxes(1.0);
  }

  // render the currently pulled vertex
  if (pulledVertex >= 0)
  {
    glColor3f(0,1,0);
    double pulledVertexPos[3];
    deformableObjectRenderingMesh->GetSingleVertexPositionFromBuffer(pulledVertex,
       &pulledVertexPos[0], &pulledVertexPos[1], &pulledVertexPos[2]);

    glEnable(GL_POLYGON_OFFSET_POINT);
    glPolygonOffset(-1.0,-1.0);
    glPointSize(8.0);
    glBegin(GL_POINTS);
    glVertex3f(pulledVertexPos[0], pulledVertexPos[1], pulledVertexPos[2]);
    glEnd();
    glDisable(GL_POLYGON_OFFSET_FILL);
  }

  // render model fixed vertices
  if (renderFixedVertices)
  {
    vector<Vec3d> uprime(volumetricMesh->getNumVertices());

    //getSurfaceVertices(&uprime);
    getSurfaceVertices(&uprime, integratorBase, volumetricMesh, n);

    //printf("\n\n\n\nFIXED ! %f %f %f\n\n\n\n", uprime[0][0], uprime[0][1], uprime[0][2]);

    for(int i=0; i<numFixedVertices; i++)
    {
      glColor3f(1,0,0);
      double fixedVertexPos[3];
      deformableObjectRenderingMesh->GetSingleVertexRestPosition(fixedVertices[i],
          &fixedVertexPos[0], &fixedVertexPos[1], &fixedVertexPos[2]);

      glEnable(GL_POLYGON_OFFSET_POINT);
      glPolygonOffset(-1.0,-1.0);
      glPointSize(7.0);
      glBegin(GL_POINTS);
      glVertex3f(fixedVertexPos[0], fixedVertexPos[1], fixedVertexPos[2]);
      glEnd();
      glDisable(GL_POLYGON_OFFSET_FILL);



    }
  }
  /*
   * ORIGINAL CODE OF MATHIAS GRYMER
   * EMAIL : mathias1292@gmail.com
   */
  if (std::strcmp(experimentType, "deformation") == 0) {
    // Plots the point for which drives the deformation Phi.
    double targetPointColorScheme[3] = {0.5, 0.9, 0};

    plotPoint(targetPlotSize, targetPointPos1, targetPointColorScheme);
  }

  if (std::strcmp(experimentType, "registration") == 0) {
    double targetPointColorScheme[3] = {0.0, 0.0, 1};
    double targetLineColorScheme[3] = {1, 0, 0};
    vector<Vec3d> pCloud;
    if (fixedTimestepGoal){
      experimentPointClouds.getPointCloud(pCloud, goalDeformationTimeStep);
      //printf("first v: %f %f %f\n",tmpVec[0][0],tmpVec[0][1],tmpVec[0][2]);
    } else {
      if (timestepCounter < experimentPointClouds.getNumPointClouds()) {

      } else {
        experimentPointClouds.getPointCloud(pCloud, experimentPointClouds.getNumPointClouds() - 1);
      }
    }
    if (renderPointCloud) {
      plotPoints(experimentPlotSize, pCloud.size(), pCloud, targetPointColorScheme);
    }
    if (renderPointCorrespondences) {
      plotPointCorrespondences(pCloud,
                               pointCorrespondencePlotLineWidt,
                               targetLineColorScheme,
                               max_point_correspondence_distance);
    }


  }

  // END OF ORIGINAL CODE

  // render springs for mass-spring systems
  if ((massSpringSystem != nullptr) & renderSprings)
  { 
    printf("rendering springs\n");
    glLineWidth(2.0);
    renderMassSprings->Render(massSpringSystem, u);
    glLineWidth(1.0);
  } 

  // ==== bitmap routines below here
  glMatrixMode(GL_MODELVIEW); 
  glPushMatrix(); 
  glLoadIdentity(); 
  glMatrixMode(GL_PROJECTION); 
  glPushMatrix(); 
  glLoadIdentity(); 

  // print info in case of integrator blow-up
  char s[4096];
  glColor3f(1,0,0);
  if (explosionFlag)
  {
    sprintf(s,"The integrator went unstable.");
    double x1 = 10;
    double y1 = 25;
    double X1 = -1 + 2.0 * x1 / windowWidth;
    double Y1 = -1 + 2.0 * y1 / windowHeight;
    print_bitmap_string(X1,Y1,-1,GLUT_BITMAP_9_BY_15 ,s);

    sprintf(s,"Reduce the timestep, or increase the number of substeps per timestep.");
    x1 = 10;
    y1 = 10;
    X1 = -1 + 2.0 * x1 / windowWidth;
    Y1 = -1 + 2.0 * y1 / windowHeight;
    print_bitmap_string(X1,Y1,-1,GLUT_BITMAP_9_BY_15 ,s);
  }

  glPopMatrix(); 
  glMatrixMode(GL_MODELVIEW); 
  glPopMatrix();

  glutSwapBuffers();
}

// Helper function for applying forces for curser based interactions.
// Input is globally tracked by GLUT library.
void mousePullForce(){
  // determine force in case user is pulling on a vertex
  if (g_iLeftMouseButton)
  {
    if (pulledVertex != -1)
    {
      double forceX = (g_vMousePos[0] - dragStartX);
      double forceY = -(g_vMousePos[1] - dragStartY);

      double externalForce[3];

      camera->CameraVector2WorldVector_OrientationOnly3D(
        forceX, forceY, 0, externalForce);

      for(int j=0; j<3; j++)
        externalForce[j] *= deformableObjectCompliance*forceFactor;

      printf("%d fx: %G fy: %G | %G %G %G\n", pulledVertex, forceX, forceY, externalForce[0], externalForce[1], externalForce[2]);

      // register force on the pulled vertex
      f_ext[3*pulledVertex+0] += externalForce[0];
      f_ext[3*pulledVertex+1] += externalForce[1];
      f_ext[3*pulledVertex+2] += externalForce[2];

      // distribute force over the neighboring vertices
      set<int> affectedVertices;
      set<int> lastLayerVertices;

      affectedVertices.insert(pulledVertex);
      lastLayerVertices.insert(pulledVertex);

      for(int j=1; j<forceNeighborhoodSize; j++)
      {
        // linear kernel
        double forceMagnitude = 1.0 * (forceNeighborhoodSize - j) / forceNeighborhoodSize;

        set<int> newAffectedVertices;
        for(set<int> :: iterator iter = lastLayerVertices.begin(); iter != lastLayerVertices.end(); iter++)
        {
          // traverse all neighbors and check if they were already previously inserted
          int vtx = *iter;
          int deg = meshGraph->GetNumNeighbors(vtx);
          for(int k=0; k<deg; k++)
          {
            int vtxNeighbor = meshGraph->GetNeighbor(vtx, k);

            if (affectedVertices.find(vtxNeighbor) == affectedVertices.end())
            {
              // discovered new vertex
              newAffectedVertices.insert(vtxNeighbor);
            }
          }
        }

        lastLayerVertices.clear();
        for(set<int> :: iterator iter = newAffectedVertices.begin(); iter != newAffectedVertices.end(); iter++)
        {
          // apply force
          f_ext[3* *iter + 0] += forceMagnitude * externalForce[0];
          f_ext[3* *iter + 1] += forceMagnitude * externalForce[1];
          f_ext[3* *iter + 2] += forceMagnitude * externalForce[2];

          // generate new layers
          lastLayerVertices.insert(*iter);
          affectedVertices.insert(*iter);
        }
      }
    }
  }
}

/*
 * ORIGINAL CODE OF MATHIAS GRYMER
 * EMAIL: mathias1292@gmail.com
 */

// Drives single point on surface mesh on object to goal point
// Can be specified as a callBack function, to drive a single surface point in any direction.
// Purposed to derive a simulated deformation of the input mesh object.
void pullForceSinglePointCallBack(){
  // Custom Force control module
  // Implemented for applying singular directional force for single vertex on surface mesh.
  if (!g_iLeftMouseButton) {

    // Interesting vertices to pick off.
    int surfaceVertex = surfaceTargetPoint;

    //double * uprime = (double *) malloc (sizeof(double) * 3*n);
    //integratorBase->GetqState(uprime);
    vector<Vec3d> uprime(volumetricMesh->getNumVertices());

    //getSurfaceVertices(&uprime);
    getSurfaceVertices(&uprime, integratorBase, volumetricMesh, n);
    Vec3d externalForce = Vec3d(0.0,0.0,0.0);
    //pointSpringForce(targetPointPos1, uprime[surfaceVertex], externalForce);
    pointSpringForce(targetPointPos1, uprime[surfaceVertex], externalForce, forceFactor, deformableObjectCompliance);
    printf("Current surface point coordinates : x %f y %f z %f\n",
           uprime[surfaceVertex][0], uprime[surfaceVertex][1], uprime[surfaceVertex][2]);
    // register force on the pulled vertex
    f_ext[3*surfaceVertex+0] += externalForce[0];
    f_ext[3*surfaceVertex+1] += externalForce[1];
    f_ext[3*surfaceVertex+2] += externalForce[2];
    // distribute force over the neighboring vertices
    set<int> affectedVertices;
    set<int> lastLayerVertices;

    affectedVertices.insert(surfaceVertex);
    lastLayerVertices.insert(surfaceVertex);

    for(int j=1; j<forceNeighborhoodSize; j++)
    {
      // linear kernel
      double forceMagnitude = 1.0 * (forceNeighborhoodSize - j) / forceNeighborhoodSize;

      set<int> newAffectedVertices;
      for(set<int> :: iterator iter = lastLayerVertices.begin(); iter != lastLayerVertices.end(); iter++)
      {
        // traverse all neighbors and check if they were already previously inserted
        int vtx = *iter;
        int deg = meshGraph->GetNumNeighbors(vtx);
        for(int k=0; k<deg; k++)
        {
          int vtxNeighbor = meshGraph->GetNeighbor(vtx, k);

          if (affectedVertices.find(vtxNeighbor) == affectedVertices.end())
          {
            // discovered new vertex
            newAffectedVertices.insert(vtxNeighbor);
          }
        }
      }

      lastLayerVertices.clear();
      for(set<int> :: iterator iter = newAffectedVertices.begin(); iter != newAffectedVertices.end(); iter++)
      {
        // apply force
        f_ext[3* *iter + 0] += forceMagnitude * externalForce[0];
        f_ext[3* *iter + 1] += forceMagnitude * externalForce[1];
        f_ext[3* *iter + 2] += forceMagnitude * externalForce[2];
        // generate new layers
        lastLayerVertices.insert(*iter);
        affectedVertices.insert(*iter);
      }
    }
  }
}

// Extracts and get deformation surface of mesh.
// And adds to experiment point cloud as a raw point cloud.
void deformationSurfacePointCloud(){
  std::vector<Vec3d> vPrime(volumetricMesh->getNumVertices());
  //getSurfaceVertices(&vPrime);
  getSurfaceVertices(&vPrime, integratorBase, volumetricMesh, n);

  experimentPointClouds.AddPointCloud(vPrime, vPrime.size());
}

// Function for designing target callback function movements.
// Can be extended upon desire to replicate some callback deformation function,
// to obtain a different target method.
void targetCallBack() {
  if ((std::strcmp(targetFunction, "circular") == 0)) {
    targetCircularHorizontalCallBack(targetCenterPos1, r, &targetPointPos1, axis1, axis2);
  }
}
// Experiement CallBack is an experiment specification for carrying out deformation registration or simulation
void experimentCallBack(){
  if(std::strcmp(experimentType, "deformation") == 0) {
    targetCallBack();
    printf("Current target coordinates : x %f y %f z %f\n", targetPointPos1[0], targetPointPos1[1], targetPointPos1[2]);
    printf("Current target center coordinates : x %f y %f z %f\n", targetCenterPos1[0], targetCenterPos1[1], targetCenterPos1[2]);

    // Single surface point on the top of mesh to follow target.
    if (surfaceTargetPoint != -1) {
      pullForceSinglePointCallBack();
    } else {

      // pre-requisites to point correspondences.
      // Synthetically generates point correspondences between all points in the mesh, and to a single moving target.
      int samples = volumetricMesh->getNumVertices();
      Vec3d *temporaryGoals;
      temporaryGoals = (Vec3d *) malloc(sizeof(Vec3d) * volumetricMesh->getNumVertices());
      int surfacePointIndices[volumetricMesh->getNumVertices()];
      for (int i = 0; i < samples; i++) {
        temporaryGoals[i] = targetPointPos1;
        surfacePointIndices[i] = i;
      }

      // Mass point to point correspondence force computation.
      if (!g_iLeftMouseButton) {
        pointToPointSpringForce(samples, surfacePointIndices, temporaryGoals, integratorBase, volumetricMesh, meshGraph,
                                f_ext, forceFactor, deformableObjectCompliance, forceNeighborhoodSize);
        printf("force: %f %f %f\n", f_ext[0], f_ext[1], f_ext[2]);
        //pointToPointSpringForce(samples, surfacePointIndices, temporaryGoals, integratorBase, meshGraph);
      }
    }
  }
  if (std::strcmp(experimentType, "registration") == 0) {

    vector<Vec3d> experimentDeformation;
    if (fixedTimestepGoal){
      experimentPointClouds.getPointCloud(experimentDeformation, goalDeformationTimeStep);
    } else {
      if (timestepCounter < experimentPointClouds.getNumPointClouds()) {
        experimentPointClouds.getPointCloud(experimentDeformation, timestepCounter);
      } else {
        experimentPointClouds.getPointCloud(experimentDeformation, experimentPointClouds.getNumPointClouds() - 1);
      }
    }

    Vec3d *temporaryGoals;
    int * surfacePointIndices;
    int numCorrespondences;

    if (false) {
      numCorrespondences = volumetricMesh->getNumVertices();
      temporaryGoals = (Vec3d *) malloc(sizeof(Vec3d) * numCorrespondences);
      surfacePointIndices = (int *) malloc(sizeof(int) * numCorrespondences);
      for (int i = 0; i < numCorrespondences; i++) {
        temporaryGoals[i] = experimentDeformation[i];
        surfacePointIndices[i] = i;
      }
    }

    //vector <Vec2i> pointCorrespondences;
    //pointCorrespondences = new vector<Vec2i>;
    vector <Vec3d> uprime(volumetricMesh->getNumVertices());
    //getSurfaceVertices(&uprime);
    getSurfaceVertices(&uprime, integratorBase, volumetricMesh, n);

    double displacementDifference = inf;
    double meansquarederror = MeanSquaredError(uprime,
                             experimentDeformation,
                             pointCorrespondences);

    if (!last_uprime.empty()) {
      displacementDifference = MeanSquaredError(uprime,
                                                last_uprime,
                                                last_correspondences);
      last_uprime = uprime;
    } else {
      last_uprime = uprime;
      for (int i = 0; i < uprime.size(); i++){
        last_correspondences.push_back(Vec2i(i,i));
      }
    }
    printf("MSE: %f displacement difference: %f\n",
      meansquarederror, displacementDifference);
    printf("Tip index %d: position %f %f %f\n",
           tipPosIndex,
           uprime[tipPosIndex][0],
           uprime[tipPosIndex][1],
           uprime[tipPosIndex][2]);
    //printf("timestep %f\n", );

    expData->addTipPos(Vec3d(uprime[tipPosIndex][0],
                             uprime[tipPosIndex][1],
                             uprime[tipPosIndex][2]));
    expData->addLoss(meansquarederror);
    expData->addDisplacement(displacementDifference);



    if (meansquarederror < epsilonMSE or
        displacementDifference < epsilonDisplacement and
        expData->timestep() > 1) {
      if (strcmp(experimentDataFileName, "__none") != 0) {
        char file_ext[4096];
        sprintf(file_ext, "statfile%d.csv",
                goalDeformationTimeStep);
        printf("%s\n", file_ext);
        saveExperiment(file_ext);
        delete expData;
        expData = new Experiment;
      }
      if (strcmp(automatedExperiment , "__none") != 0) {
        resetExperiment();
      } else {
        pauseSimulation = !pauseSimulation;
        singleStepMode = 0;
      }
    }

    pointCorrespondences.clear();
    closestPointsKDTree(uprime, experimentDeformation,
                        &pointCorrespondences, &numCorrespondences,
                        max_point_correspondence_distance);
    if (std::strcmp(registrationModel, "springForceModel") == 0) {
      /*
      nearestNeighbor(uprime, experimentDeformation,
                      &pointCorrespondences, &numCorrespondences,
                      max_point_correspondence_distance); */
      temporaryGoals = (Vec3d *) malloc(sizeof(Vec3d) * numCorrespondences);
      surfacePointIndices = (int *) malloc(sizeof(int) * numCorrespondences);
      for (int i = 0; i < numCorrespondences; i++) {
        temporaryGoals[i] = experimentDeformation[pointCorrespondences[i][1]];
        surfacePointIndices[i] = pointCorrespondences[i][0];

      }

      if (!g_iLeftMouseButton) {
        pointToPointSpringForce(numCorrespondences, surfacePointIndices,
                                temporaryGoals, integratorBase,
                                volumetricMesh, meshGraph,
                                f_ext, forceFactor, deformableObjectCompliance,
                                forceNeighborhoodSize);
      }
    }

    if (std::strcmp(registrationModel, "displacementModel") == 0) {
      // Bring closest pair of points directly.
      /*
      double * displacements = (double *) malloc (sizeof(double) * 3*n);
      integratorBase->GetqState(displacements);
      vector<Vec3d> surfaceVertices(volumetricMesh->getNumVertices());
      //double * surfaceVertices = (double *) malloc (sizeof(double) * 3 * n);
      volumetricMesh->exportMeshGeometry(surfaceVertices);
      Vec3d direction;
      double dist;
      double distanceFactor = 1.0/10.0;
      for (int i = 0; i < pointCorrespondences.size(); i++) {
        int surface_idx = pointCorrespondences[i][0];
        int pcloud_idx = pointCorrespondences[i][1];
        Vec3d position = uprime[surface_idx];
        Vec3d goal = experimentDeformation[pcloud_idx];
        norm(goal, position, &direction, &dist);
        uprime[surface_idx][0] = uprime[surface_idx][0] + direction[0]*dist*distanceFactor;
        uprime[surface_idx][1] = uprime[surface_idx][1] + direction[1]*dist*distanceFactor;
        uprime[surface_idx][2] = uprime[surface_idx][2] + direction[2]*dist*distanceFactor;
      }
      for (int i = 0; i < 3*n; i++){
        displacements[i] = uprime[i/3][i%3] - surfaceVertices[i/3][i%3];
      }
      integratorBase->SetqState(displacements);
       */
      applyDisplacement(integratorBase,
                        volumetricMesh,
                        pointCorrespondences,
                        uprime,
                        experimentDeformation,
                        meshGraph,
                        forceNeighborhoodSize);
    }
  }
}
// END OF ORIGINAL CODE

// called periodically by GLUT:
void idleFunction(void)
{
  cpuLoadCounter.StartCounter();

  glutSetWindow(windowID);

  // reset external forces (usually to zero)
  memcpy(f_ext, f_extBase, sizeof(double) * 3 * n);

  if ((!lockScene) && (!pauseSimulation) && (singleStepMode <= 1))
  {
    /*
     * ORIGINAL CODE OF MATHIAS GRYMER
     * EMAIL: mathias1292@gmail.com
     */

    // Allows user to induce mouse forces to single surface point.
    // Simple drag and drop force.
    mousePullForce();
    // Applies forces depending on global variables.
    experimentCallBack();
    // END OF ORIGINAL CODE

    // apply any scripted force loads
    if (timestepCounter < numForceLoads)
    {
      printf("  External forces read from the binary input file.\n");
      for(int i=0; i<3*n; i++)
        f_ext[i] += forceLoads[ELT(3*n, i, timestepCounter)];
    }

    // set forces to the integrator
    integratorBaseSparse->SetExternalForces(f_ext);

    PerformanceCounter totalDynamicsCounter;

    // timestep the dynamics 
    for(int i=0; i<substepsPerTimeStep; i++)
    {
      int code = integratorBase->DoTimestep();
      //printf(".");
      fflush(nullptr);

      double forceAssemblyLocalTime = integratorBaseSparse->GetForceAssemblyTime();
      double systemSolveLocalTime = integratorBaseSparse->GetSystemSolveTime();
      //printf("Force assembly: %G\nSystem solve: %G\n", forceAssemblyTime, systemSolveTime);

      forceAssemblyBuffer.addValue(forceAssemblyLocalTime);
      forceAssemblyTime = forceAssemblyBuffer.getAverage();

      systemSolveBuffer.addValue(systemSolveLocalTime);
      systemSolveTime = systemSolveBuffer.getAverage();

      if (code != 0)
      {
        printf("The integrator went unstable. Reduce the timestep, or increase the number of substeps per timestep.\n");
        integratorBase->ResetToRest();
        for(int i=0; i<3*n; i++)
        {
          f_ext[i] = 0.0;
          f_extBase[i] = 0.0;
        }
        integratorBase->SetExternalForcesToZero();
        explosionFlag = 1;
        explosionCounter.StartCounter();
        break;
      }

      // update UI performance indicators
      if (strcmp(outputFilename, "__none") != 0)
      {
        char s[4096];
        sprintf(s, "%s.u.%04d", outputFilename, subTimestepCounter);
        printf("Saving deformation to %s.\n", s);
        WriteMatrixToDisk_(s, 3*n, 1, integratorBase->Getq());
        sprintf(s, "%s.f.%04d", outputFilename, subTimestepCounter);
        printf("Saving forces to %s.\n", s);
        WriteMatrixToDisk_(s, 3*n, 1, integratorBase->GetExternalForces());
      }
      if (strcmp(experimentType, "deformation") == 0) {
        deformationSurfacePointCloud();
      }

      printf("timestep %d\n", substepsPerTimeStep*timestepCounter+i);
      expData->addTime(substepsPerTimeStep*timestepCounter+i);

      if (!timerSet) {
        start = chrono::system_clock::now();
        timerSet = true;
      }
      chrono::time_point<std::chrono::system_clock> end = chrono::system_clock::now();
      chrono::duration<double> elapsed_seconds = end - start;
      printf("elapsed time: %f\n", elapsed_seconds.count());
      expData->addRealTime(elapsed_seconds.count());

      subTimestepCounter++;
    }

    timestepCounter++;

    totalDynamicsCounter.StopCounter();
    //printf("Total dynamics: %G\n", totalDynamicsCounter.GetElapsedTime());

    memcpy(u, integratorBase->Getq(), sizeof(double) * 3 * n);

    if (singleStepMode == 1)
      singleStepMode = 2;

    //printf("F");

    fflush(nullptr);
    graphicFrame++;

    if (lockAt30Hz)
    {
      double elapsedTime;
      do
      {
        titleBarCounter.StopCounter();
        elapsedTime = titleBarCounter.GetElapsedTime();
      }
      while (1.0 * graphicFrame / elapsedTime >= 30.0);
    }
  }

  deformableObjectRenderingMesh->SetVertexDeformations(u);

  // interpolate deformations from volumetric mesh to rendering triangle mesh
  if (secondaryDeformableObjectRenderingMesh != nullptr)
  {
    PerformanceCounter interpolationCounter;
    VolumetricMesh::interpolate(u, uSecondary, secondaryDeformableObjectRenderingMesh->Getn(), secondaryDeformableObjectRenderingMesh_interpolation_numElementVertices, secondaryDeformableObjectRenderingMesh_interpolation_vertices, secondaryDeformableObjectRenderingMesh_interpolation_weights);
    secondaryDeformableObjectRenderingMesh->SetVertexDeformations(uSecondary);
    interpolationCounter.StopCounter();
    //printf("Interpolate deformations: %G\n", interpolationCounter.GetElapsedTime());
  }

  if (useRealTimeNormals)
  {
    // recompute normals
    PerformanceCounter normalsCounter;
    deformableObjectRenderingMesh->BuildNormals(); 
    if (secondaryDeformableObjectRenderingMesh != nullptr)
      secondaryDeformableObjectRenderingMesh->BuildNormals();
    normalsCounter.StopCounter();
    //printf("Recompute normals: %G\n", normalsCounter.GetElapsedTime());
  }

  if (explosionFlag)
  {
    explosionCounter.StopCounter();
    if (explosionCounter.GetElapsedTime() > 4.0) // the message will appear on screen for 4 seconds
      explosionFlag = 0;
  }

  // update window title at 5 Hz
  titleBarCounter.StopCounter();
  double elapsedTime = titleBarCounter.GetElapsedTime();
  if (elapsedTime >= 1.0 / 5)
  {
    titleBarCounter.StartCounter();
    double fpsLocal = graphicFrame / elapsedTime;
    fpsBuffer.addValue(fpsLocal);
    fps = fpsBuffer.getAverage();

    //printf("Frames per second: %G\n", fps);

    // update window title 
    char windowTitle[4096] = "unknown defo model";

    if (deformableObject == STVK)
      sprintf(windowTitle,"%s | %s | Elements: %d | DOFs: %d | %.1f Hz | Defo CPU Load: %d%%", "StVK", solverMethod, volumetricMesh->getNumElements(), volumetricMesh->getNumVertices() * 3, fps, (int)(100 * cpuLoad + 0.5) );

    if (deformableObject == COROTLINFEM)
      sprintf(windowTitle,"%s:%d | %s | Elements: %d | DOFs: %d | %.1f Hz | Defo CPU Load: %d%%", "CLFEM", corotationalLinearFEM_warp, solverMethod, volumetricMesh->getNumElements(), volumetricMesh->getNumVertices() * 3, fps, (int)(100 * cpuLoad + 0.5) );

    if (deformableObject == LINFEM)
      sprintf(windowTitle,"%s | %s | Elements: %d | DOFs: %d | %.1f Hz | Defo CPU Load: %d%%", "LinFEM", solverMethod, volumetricMesh->getNumElements(), volumetricMesh->getNumVertices() * 3, fps, (int)(100 * cpuLoad + 0.5) );

    if (deformableObject == MASSSPRING)
      sprintf(windowTitle,"%s | %s | Particles: %d | Edges: %d | %.1f Hz | Defo CPU Load: %d%%", "mass-spring", solverMethod, massSpringSystem->GetNumParticles(), massSpringSystem->GetNumEdges(), fps, (int)(100 * cpuLoad + 0.5) );

    if (deformableObject == INVERTIBLEFEM)
    {
      char materialType[96] = "Invertible FEM";

      if (invertibleMaterial == INV_STVK)
        strcpy(materialType, "Invertible StVK");

      if (invertibleMaterial == INV_NEOHOOKEAN)
        strcpy(materialType, "Invertible neo-Hookean");

      if (invertibleMaterial == INV_MOONEYRIVLIN)
        strcpy(materialType, "Invertible Mooney-Rivlin");

      sprintf(windowTitle,"%s | %s | Elements: %d | DOFs: %d | %.1f Hz | Defo CPU Load: %d%%", materialType, solverMethod, volumetricMesh->getNumElements(), volumetricMesh->getNumVertices() * 3, fps, (int)(100 * cpuLoad + 0.5) );
    }

    glutSetWindowTitle(windowTitle);
    graphicFrame = 0;

    char ptext1[96];
    sprintf(ptext1, "Force assembly: %G", forceAssemblyTime);
    //forceAssemblyStaticText->set_text(ptext1);
    char solverName[96];
    GetIntegratorSolver(solverName);
    char ptext2[96];
    sprintf(ptext2, "System solve (%s): %G", solverName, systemSolveTime);
    //systemSolveStaticText->set_text(ptext2);
    //Sync_GLUI();

    if ((syncTimestepWithGraphics) && ((!lockScene) && (!pauseSimulation) && (singleStepMode == 0)))
    {
      timeStep = 1.0 / fps;
      integratorBase->SetTimestep(timeStep / substepsPerTimeStep);
      //Sync_GLUI();
    }
  }

  // Termination test for predefined experiment timer.
  testExperimentExpired();

  cpuLoadCounter.StopCounter();
  double cpuTimePerGraphicsFrame = cpuLoadCounter.GetElapsedTime();
  cpuLoad = cpuTimePerGraphicsFrame * fps; 

  glutPostRedisplay();
}



// reacts to pressed keys
void keyboardFunction(unsigned char key, int x, int y)
{
  switch (key) {
    case 27:
      exit(0);

    case 13:
      if (singleStepMode == 2)
        singleStepMode = 1;
      break;

    case 'w':
      renderWireframe = !renderWireframe;
      break;

    case 'c':
      renderPointCorrespondences = !renderPointCorrespondences;
      break;

    case 'C':
      renderPointCloud = !renderPointCloud;
      break;

    case 's':
      renderSurfaceMesh = !renderSurfaceMesh;
      break;

    case 'i': {
      double focusPos[3];
      camera->GetFocusPosition(focusPos);
      double cameraX, cameraY, cameraZ;
      camera->GetAbsWorldPosition(cameraX, cameraY, cameraZ);
      printf("Camera is positioned at: %G %G %G\n", cameraX, cameraY, cameraZ);
      printf("Camera radius is: %G\n", camera->GetRadius());
      printf("Camera Phi is: %G\n", 180.0 / M_PI * camera->GetPhi());
      printf("Camera Theta is: %G\n", 180.0 / M_PI * camera->GetTheta());
      printf("Camera focus is: %G %G %G\n", focusPos[0], focusPos[1], focusPos[2]);
      printf("ConfigPaster\n");
      printf("*cameraRadius\n%G\n", camera->GetRadius());
      printf("*cameraLongitude\n%G\n", 180.0 / M_PI * camera->GetPhi());
      printf("*cameraLattitude\n%G\n", 180.0 / M_PI * camera->GetTheta());
      printf("*focusPositionX\n%G\n", focusPos[0]);
      printf("*focusPositionY\n%G\n", focusPos[1]);
      printf("*focusPositionZ\n%G\n", focusPos[2]);
    }
    break;

    case '\\':
      camera->Reset();
      break;

    case 'a':
      renderAxes = !renderAxes;
      break;


    case 'b':
      renderFixedVertices = !renderFixedVertices;
      break;

    case 'B': {
      int num = experimentPointClouds.getNumPointClouds();
      printf("Number of point clouds: %d\n", num);
      printf("Current point cloud: %d\n", goalDeformationTimeStep);
      vector <Vec3d> vec;
      experimentPointClouds.getPointCloud(vec, 0);
      printf("Current point cloud size: %lu\n", vec.size());
      for (int i = 0; i < 10; i++) {
        printf("points %f %f %f\n", vec[i][0],
               vec[i][1],
               vec[i][2]);
      }
    }
      break;

    case 'v':
      renderSprings = !renderSprings;
      break;

    case 'V':
      renderVertices = !renderVertices;
      break;

    case 'L':
      lockAt30Hz = !lockAt30Hz;
      break;

    case 'n':
      if (goalDeformationTimeStep < experimentPointClouds.getNumPointClouds()) {
        resetExperiment();
      }
      break;

      // in this mode, can move the camera while the object's deformations are frozen
    case 'l':
      lockScene = !lockScene;
      if (lockScene) {
        camera->PushPosition();
      } else {
        camera->PopPosition();
      }
      break;

    case '1':
      corotationalLinearFEM_warp = (corotationalLinearFEM_warp + 1) % (max_corotationalLinearFEM_warp + 1);
      if (corotationalLinearFEMStencilForceModel != nullptr)
        corotationalLinearFEMStencilForceModel->SetWarp(corotationalLinearFEM_warp);

      printf("CorotationalLinearFEM warp is now: %d\n", corotationalLinearFEM_warp);
      break;

    case 'e':
      renderDeformableObject = !renderDeformableObject;
      break;

    case 'E':
      renderSecondaryDeformableObject = !renderSecondaryDeformableObject;
      if (secondaryDeformableObjectRenderingMesh == nullptr)
        renderSecondaryDeformableObject = 0;
      break;

    case 'N':
      useRealTimeNormals = !useRealTimeNormals;
      break;

    case 'p':
      pauseSimulation = !pauseSimulation;
      singleStepMode = 0;
      break;

    case 'P':
      if (singleStepMode > 0)
        singleStepMode = 0;
      else
        singleStepMode = 1;
      break;

    case 'u':
      // save the current deformation to a disk file
      printf("Writing defomations to out.u .\n");
      WriteMatrixToDisk("out.u", 3 * n, 1, u);
      break;

    case 'z':
      for (int i = 0; i < 3 * n; i++)
        f_extBase[i] = f_ext[i];
      dragStartX = g_vMousePos[0];
      dragStartY = g_vMousePos[1];
      break;

    case 'Z':
      for (int i = 0; i < 3 * n; i++)
        f_extBase[i] = 0;
      dragStartX = g_vMousePos[0];
      dragStartY = g_vMousePos[1];
      break;

    case '0':
      timerSet = false;
      delete expData;
      expData = new Experiment;
      stopDeformations_buttonCallBack(0);
      pointCorrespondences.clear();
      break;
  }
}

// reacts to pressed "special" keys
void specialFunction(int key, int x, int y)
{
  switch (key)
  {
    case GLUT_KEY_LEFT:
      camera->MoveFocusRight(+0.1 * fabs(camera->GetRadius()));
    break;

    case GLUT_KEY_RIGHT:
      camera->MoveFocusRight(-0.1 * fabs(camera->GetRadius()));
    break;

    case GLUT_KEY_DOWN:
      camera->MoveFocusUp(+0.1 * fabs(camera->GetRadius()));
    break;

    case GLUT_KEY_UP:
      camera->MoveFocusUp(-0.1 * fabs(camera->GetRadius()));
    break;

    case GLUT_KEY_PAGE_UP:
      break;

    case GLUT_KEY_PAGE_DOWN:
      break;

    case GLUT_KEY_HOME:
      break;

    case GLUT_KEY_END:
      break;

    case GLUT_KEY_INSERT:
      break;

    default:
      break;
  }
}

void reshape(int x, int y)
{
  glViewport(0,10,x,y);

  windowWidth = x;
  windowHeight = y;

  glMatrixMode(GL_PROJECTION); 
  glLoadIdentity(); 

  // compute the window aspect ratio 
  gluPerspective(45.0f, 1.0 * windowWidth / windowHeight, zNear, zFar);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

// reacts to mouse motion
void mouseMotionFunction(int x, int y)
{
  int mouseDeltaX = x-g_vMousePos[0];
  int mouseDeltaY = y-g_vMousePos[1];

  g_vMousePos[0] = x;
  g_vMousePos[1] = y;

  if (g_iLeftMouseButton) // left mouse button 
  {
  }

  if (g_iRightMouseButton) // right mouse button handles camera rotations
  {
    const double factor = 0.2;

    camera->MoveRight(factor * mouseDeltaX);
    camera->MoveUp(factor * mouseDeltaY);
  }

  if ((g_iMiddleMouseButton) || (g_iLeftMouseButton && altPressed)) // handle zoom in/out
  {
    const double factor = 0.1;
    camera->ZoomIn(cameraRadius * factor * mouseDeltaY);
  }
}

// reacts to pressed mouse buttons
void mouseButtonActivityFunction(int button, int state, int x, int y)
{
  switch (button)
  {
    case GLUT_LEFT_BUTTON:
      g_iLeftMouseButton = (state==GLUT_DOWN);
      shiftPressed = (glutGetModifiers() == GLUT_ACTIVE_SHIFT);
      altPressed = (glutGetModifiers() == GLUT_ACTIVE_ALT);
      ctrlPressed = (glutGetModifiers() == GLUT_ACTIVE_CTRL);

      if ((g_iLeftMouseButton) && (!shiftPressed) && (!ctrlPressed))
      {
        // apply force to vertex

        GLdouble model[16];
        glGetDoublev (GL_MODELVIEW_MATRIX, model);

        GLdouble proj[16];
        glGetDoublev (GL_PROJECTION_MATRIX, proj);

        GLint view[4];
        glGetIntegerv (GL_VIEWPORT, view);

        int winX = x;
        int winY = view[3]-1-y;

        float zValue;
        glReadPixels(winX,winY,1,1, GL_DEPTH_COMPONENT, GL_FLOAT, &zValue);

        GLubyte stencilValue;
        glReadPixels(winX, winY, 1, 1, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, &stencilValue);

        GLdouble worldX, worldY, worldZ;
        gluUnProject(winX, winY, zValue, model, proj, view, &worldX, &worldY, &worldZ);

        if (stencilValue == 1)
        {
          dragStartX = x;
          dragStartY = y;
          Vec3d pos(worldX, worldY, worldZ);
          pulledVertex = deformableObjectRenderingMesh->GetClosestVertex(pos);
          printf("Clicked on vertex: %d (0-indexed)\n", pulledVertex);
        }
        else
        {
          printf("Clicked on empty stencil: %d.\n", stencilValue);
        }
      }

      if (!g_iLeftMouseButton)
      {
        pulledVertex = -1;
      }

      break;

    case GLUT_MIDDLE_BUTTON:
      g_iMiddleMouseButton = (state==GLUT_DOWN);
      shiftPressed = (glutGetModifiers() == GLUT_ACTIVE_SHIFT);
      ctrlPressed = (glutGetModifiers() == GLUT_ACTIVE_CTRL);
      break;

    case GLUT_RIGHT_BUTTON:
      g_iRightMouseButton = (state==GLUT_DOWN);
      shiftPressed = (glutGetModifiers() == GLUT_ACTIVE_SHIFT);
      ctrlPressed = (glutGetModifiers() == GLUT_ACTIVE_CTRL);
      break;
  }

  g_vMousePos[0] = x;
  g_vMousePos[1] = y;
}

// program initialization
void initSimulation()
{
  // init lighting
  try
  {
    lighting = new Lighting(lightingConfigFilename);
  }
  catch(int exceptionCode)
  {
    printf("Error (%d) reading lighting information from %s .\n",exceptionCode, lightingConfigFilename);
    exit(1);
  }

  // init camera
  delete(camera);
  double virtualToPhysicalPositionFactor = 1.0;
  initCamera(cameraRadius, cameraLongitude, cameraLattitude,
     focusPositionX, focusPositionY, focusPositionZ,
     1.0 / virtualToPhysicalPositionFactor,
     &zNear, &zFar, &camera);

  volumetricMesh = nullptr;
  massSpringSystem = nullptr;

  // set deformable material type
  if (strcmp(volumetricMeshFilename, "__none") != 0)
  {
    if (strcmp(deformableObjectMethod, "StVK") == 0)
      deformableObject = STVK;
    if (strcmp(deformableObjectMethod, "CLFEM") == 0)
      deformableObject = COROTLINFEM;
    if (strcmp(deformableObjectMethod, "LinearFEM") == 0)
      deformableObject = LINFEM;
    if (strcmp(deformableObjectMethod, "InvertibleFEM") == 0)
      deformableObject = INVERTIBLEFEM;
  }

  if (strcmp(massSpringSystemObjConfigFilename, "__none") != 0)
    massSpringSystemSource = OBJ;
  else if (strcmp(massSpringSystemTetMeshConfigFilename, "__none") != 0)
    massSpringSystemSource = TETMESH;
  else if (strcmp(massSpringSystemCubicMeshConfigFilename, "__none") != 0)
    massSpringSystemSource = CUBICMESH;
  else if (strncmp(customMassSpringSystem, "chain", 5) == 0)
    massSpringSystemSource = CHAIN;

  if ((massSpringSystemSource == OBJ) || (massSpringSystemSource == TETMESH) || (massSpringSystemSource == CUBICMESH) || (massSpringSystemSource == CHAIN)) 
    deformableObject = MASSSPRING;

  if (deformableObject == UNSPECIFIED)
  {
    printf("Error: no deformable model specified.\n");
    exit(1);
  }

  // load volumetric mesh
  if ((deformableObject == STVK) || (deformableObject == COROTLINFEM) || (deformableObject == LINFEM) || (deformableObject == INVERTIBLEFEM))
  {
    printf("Loading volumetric mesh from file %s...\n", volumetricMeshFilename);

    VolumetricMesh::fileFormatType fileFormat = VolumetricMesh::ASCII;
    int verbose = 0;
    volumetricMesh = VolumetricMeshLoader::load(volumetricMeshFilename, fileFormat, verbose);
    if (volumetricMesh == nullptr)
    {
      printf("Error: unable to load the volumetric mesh from %s.\n", volumetricMeshFilename);
      exit(1);
    }

    n = volumetricMesh->getNumVertices();
    printf("Num vertices: %d. Num elements: %d\n", n, volumetricMesh->getNumElements());
    meshGraph = GenerateMeshGraph::Generate(volumetricMesh);

    // create the mass matrix
    int inflate3Dim = true; // necessary so that the returned matrix is 3n x 3n
    GenerateMassMatrix::computeMassMatrix(volumetricMesh, &massMatrix, inflate3Dim);

    // create the internal forces for STVK and linear FEM materials
    if (deformableObject == STVK || deformableObject == LINFEM)  // LINFEM is constructed from stVKInternalForces
    {
      unsigned int loadingFlag = 0; // 0 = use the low-memory version, 1 = use the high-memory version
      StVKElementABCD * precomputedIntegrals = StVKElementABCDLoader::load(volumetricMesh, loadingFlag);
      if (precomputedIntegrals == nullptr)
      {
        printf("Error: unable to load the StVK integrals.\n");
        exit(1);
      }

      printf("Generating internal forces and stiffness matrix models...\n"); fflush(nullptr);
      stVKFEM = new StVKFEM(volumetricMesh, precomputedIntegrals, addGravity, g);
    }
  }

  // load mass spring system (if any) 
  if (deformableObject == MASSSPRING)
  {
    switch (massSpringSystemSource)
    {
    case OBJ:
    {
      printf("Loading mass spring system from an obj file...\n");
      MassSpringSystemFromObjMeshConfigFile massSpringSystemFromObjMeshConfigFile;
      MassSpringSystemObjMeshConfiguration massSpringSystemObjMeshConfiguration;
      if (massSpringSystemFromObjMeshConfigFile.GenerateMassSpringSystem(massSpringSystemObjConfigFilename, &massSpringSystem, &massSpringSystemObjMeshConfiguration) != 0)
      {
        printf("Error initializing the mass spring system.\n");
        exit(1);
      }
      strcpy(renderingMeshFilename, massSpringSystemObjMeshConfiguration.massSpringMeshFilename);
    }
    break;

    case TETMESH:
    {
      printf("Loading mass spring system from a tet mesh file...\n");
      MassSpringSystemFromTetMeshConfigFile massSpringSystemFromTetMeshConfigFile;
      MassSpringSystemTetMeshConfiguration massSpringSystemTetMeshConfiguration;
      if (massSpringSystemFromTetMeshConfigFile.GenerateMassSpringSystem(massSpringSystemTetMeshConfigFilename, &massSpringSystem, &massSpringSystemTetMeshConfiguration) != 0)
      {
        printf("Error initializing the mass spring system.\n");
        exit(1);
      }
      strcpy(renderingMeshFilename, massSpringSystemTetMeshConfiguration.surfaceMeshFilename);
    }
    break;

    case CUBICMESH:
    {
      printf("Loading mass spring system from a cubic mesh file...\n");
      MassSpringSystemFromCubicMeshConfigFile massSpringSystemFromCubicMeshConfigFile;
      MassSpringSystemCubicMeshConfiguration massSpringSystemCubicMeshConfiguration;
      if (massSpringSystemFromCubicMeshConfigFile.GenerateMassSpringSystem(massSpringSystemCubicMeshConfigFilename, &massSpringSystem, &massSpringSystemCubicMeshConfiguration) != 0)
      {
        printf("Error initializing the mass spring system.\n");
        exit(1);
      }
      strcpy(renderingMeshFilename, massSpringSystemCubicMeshConfiguration.surfaceMeshFilename);
    }
    break;

    case CHAIN:
    {
      int numParticles;
      double groupStiffness;
      sscanf(customMassSpringSystem, "chain,%d,%lf", &numParticles, &groupStiffness);
      printf("Creating a chain mass-spring system with %d particles...\n", numParticles);

      double * masses = (double*) malloc (sizeof(double) * numParticles);
      for(int i=0; i<numParticles; i++)
        masses[i] = 1.0;

      double * restPositions = (double*) malloc (sizeof(double) * 3 * numParticles);
      for(int i=0; i<numParticles; i++)
      {
        restPositions[3*i+0] = 0;
        restPositions[3*i+1] = (numParticles == 1) ? 0.0 : 1.0 * i / (numParticles-1);
        restPositions[3*i+2] = 0;
      }
      int * edges = (int*) malloc (sizeof(int) * 2 * (numParticles - 1));
      for(int i=0; i<numParticles-1; i++)
      {
        edges[2*i+0] = i;
        edges[2*i+1] = i+1;
      }

      int * edgeGroups = (int*) malloc (sizeof(int) * (numParticles - 1));
      for(int i=0; i<numParticles-1; i++)
        edgeGroups[i] = 0;
      double groupDamping = 0;

      massSpringSystem = new MassSpringSystem(numParticles, masses, restPositions, numParticles - 1, edges, edgeGroups, 1, &groupStiffness, &groupDamping, addGravity);

      char s[96];
      sprintf(s,"chain-%d.obj", numParticles);
      massSpringSystem->CreateObjMesh(s);
      strcpy(renderingMeshFilename, s);

      free(edgeGroups);
      free(edges);
      free(restPositions);
      free(masses);

      renderVertices = 1;
    }
    break;

    default:
      printf("Error: mesh spring system configuration file was not specified.\n");
      exit(1);
      break;
    }

    if (addGravity)
      massSpringSystem->SetGravity(addGravity, g);

    n = massSpringSystem->GetNumParticles();

    // create the mass matrix
    massSpringSystem->GenerateMassMatrix(&massMatrix);

    // create the mesh graph (used only for the distribution of user forces over neighboring vertices)
    meshGraph = new Graph(massSpringSystem->GetNumParticles(), massSpringSystem->GetNumEdges(), massSpringSystem->GetEdges());
  }

  int scaleRows = 1;
  meshGraph->GetLaplacian(&LaplacianDampingMatrix, scaleRows);
  LaplacianDampingMatrix->ScalarMultiply(dampingLaplacianCoef);

  // initialize the rendering mesh for the volumetric mesh
  if (strcmp(renderingMeshFilename, "__none") == 0)
  {
    printf("Error: rendering mesh was not specified.\n");
    exit(1);
  }
  deformableObjectRenderingMesh = new SceneObjectDeformable(renderingMeshFilename);
  if (enableTextures)
    deformableObjectRenderingMesh->SetUpTextures(SceneObject::MODULATE, SceneObject::NOMIPMAP);
  deformableObjectRenderingMesh->ResetDeformationToRest();
  deformableObjectRenderingMesh->BuildNeighboringStructure();
  deformableObjectRenderingMesh->BuildNormals(); 
  deformableObjectRenderingMesh->SetMaterialAlpha(0.5);

  // initialize the embedded triangle rendering mesh 
  secondaryDeformableObjectRenderingMesh = nullptr;
  if (strcmp(secondaryRenderingMeshFilename, "__none") != 0)
  {
    secondaryDeformableObjectRenderingMesh = new SceneObjectDeformable(secondaryRenderingMeshFilename);
    if (enableTextures)
      secondaryDeformableObjectRenderingMesh->SetUpTextures(SceneObject::MODULATE, SceneObject::NOMIPMAP);
    secondaryDeformableObjectRenderingMesh->ResetDeformationToRest();
    secondaryDeformableObjectRenderingMesh->BuildNeighboringStructure();
    secondaryDeformableObjectRenderingMesh->BuildNormals(); 

    uSecondary = (double*) calloc (3 * secondaryDeformableObjectRenderingMesh->Getn(), sizeof(double));

    // load interpolation structure
    if (strcmp(secondaryRenderingMeshInterpolationFilename, "__none") == 0)
    {
      printf("Error: no secondary rendering mesh interpolation filename specified.\n");
      exit(1);
    }

    secondaryDeformableObjectRenderingMesh_interpolation_numElementVertices = VolumetricMesh::getNumInterpolationElementVertices(secondaryRenderingMeshInterpolationFilename);

    if (secondaryDeformableObjectRenderingMesh_interpolation_numElementVertices < 0)
    {
      printf("Error: unable to open file %s.\n", secondaryRenderingMeshInterpolationFilename);
      exit(1);
    }

    printf("Num interpolation element vertices: %d\n", secondaryDeformableObjectRenderingMesh_interpolation_numElementVertices);

    VolumetricMesh::loadInterpolationWeights(secondaryRenderingMeshInterpolationFilename, secondaryDeformableObjectRenderingMesh->Getn(), secondaryDeformableObjectRenderingMesh_interpolation_numElementVertices, &secondaryDeformableObjectRenderingMesh_interpolation_vertices, &secondaryDeformableObjectRenderingMesh_interpolation_weights);
  }
  else
    renderSecondaryDeformableObject = 0;

  if (!((deformableObject == MASSSPRING) && (massSpringSystemSource == CHAIN)))
  {
    // read the fixed vertices
    // 1-indexed notation
    if (strcmp(fixedVerticesFilename, "__none") == 0)
    {
      numFixedVertices = 0;
      fixedVertices = nullptr;
    }
    else
    {
      if (ListIO::load(fixedVerticesFilename, &numFixedVertices,&fixedVertices) != 0)
      {
        printf("Error reading fixed vertices.\n");
        exit(1);
      }
      ListIO::sort(numFixedVertices, fixedVertices);
    }
  }
  else
  {
    numFixedVertices = 1;
    fixedVertices = (int*) malloc (sizeof(int) * (1+numFixedVertices));
    fixedVertices[0] = massSpringSystem->GetNumParticles();
  }

  printf("Loaded %d fixed vertices. They are:\n",numFixedVertices);
  ListIO::print(numFixedVertices,fixedVertices);
  // create 0-indexed fixed DOFs
  int numFixedDOFs = 3 * numFixedVertices;
  int * fixedDOFs = (int*) malloc (sizeof(int) * numFixedDOFs);

  for(int i=0; i<numFixedVertices; i++)
  {
    fixedDOFs[3*i+0] = 3*fixedVertices[i]-3;
    fixedDOFs[3*i+1] = 3*fixedVertices[i]-2;
    fixedDOFs[3*i+2] = 3*fixedVertices[i]-1;
  }
  for(int i=0; i<numFixedVertices; i++)
    fixedVertices[i]--;
  printf("Boundary vertices processed.\n");

  // make room for deformation and force vectors
  u = (double*) calloc (3*n, sizeof(double));
  uvel = (double*) calloc (3*n, sizeof(double));
  uaccel = (double*) calloc (3*n, sizeof(double));
  f_ext = (double*) calloc (3*n, sizeof(double));
  f_extBase = (double*) calloc (3*n, sizeof(double));

  // load initial condition
  if (strcmp(initialPositionFilename, "__none") != 0)
  {
    int m1, n1;
    ReadMatrixFromDisk_(initialPositionFilename, &m1, &n1, &uInitial);
    if ((m1 != 3*n) || (n1 != 1))
    {
      printf("Error: initial position matrix size mismatch.\n");
      exit(1);
    }
  }
  else if ((deformableObject == MASSSPRING) && (massSpringSystemSource == CHAIN))
  {
    uInitial = (double*) calloc (3*n, sizeof(double));
    int numParticles = massSpringSystem->GetNumParticles(); 
    for(int i=0; i<numParticles; i++)
    {
      uInitial[3*i+0] = 1.0 - ((numParticles == 1) ? 1.0 : 1.0 * i / (numParticles - 1));
      uInitial[3*i+1] = 1.0 - ((numParticles == 1) ? 0.0 : 1.0 * i / (numParticles-1));
      uInitial[3*i+2] = 0.0;
    }
  }
  else
    uInitial = (double*) calloc (3*n, sizeof(double));

  // load initial velocity
  if (strcmp(initialVelocityFilename, "__none") != 0)
  {
    int m1, n1;
    ReadMatrixFromDisk_(initialVelocityFilename, &m1, &n1, &velInitial);
    if ((m1 != 3*n) || (n1 != 1))
    {
      printf("Error: initial position matrix size mismatch.\n");
      exit(1);
    }
  }

  // load force loads
  if (strcmp(forceLoadsFilename, "__none") != 0)
  {
    int m1;
    ReadMatrixFromDisk_(forceLoadsFilename, &m1, &numForceLoads, &forceLoads);
    if (m1 != 3*n)
    {
      printf("Mismatch in the dimension of the force load matrix.\n");
      exit(1);
    }
  }

  // create force model, to be used by the integrator
  printf("Creating force model...\n");
  if (deformableObject == STVK)
  {
    printf("Force model: STVK\n");
    fflush(stdout);

    stVKStencilForceModel = new StVKStencilForceModel(stVKFEM);
    stencilForceModel = stVKStencilForceModel;
  }

  if (deformableObject == COROTLINFEM)
  {
    printf("Force model: COROTLINFEM\n");

    CorotationalLinearFEM * corotationalLinearFEM = new CorotationalLinearFEM(volumetricMesh);

    corotationalLinearFEMStencilForceModel = new CorotationalLinearFEMStencilForceModel(corotationalLinearFEM);
    corotationalLinearFEMStencilForceModel->SetWarp(corotationalLinearFEM_warp);

    stencilForceModel = corotationalLinearFEMStencilForceModel;
  }

  if (deformableObject == LINFEM)
  {
    printf("Force model: LINFEM\n");

    assert(stVKStencilForceModel != nullptr);
    linearFEMStencilForceModel = new LinearFEMStencilForceModel(stVKStencilForceModel);
    stencilForceModel = linearFEMStencilForceModel;
  }

  if (deformableObject == INVERTIBLEFEM)
  {
    printf("Force model: INVERTIBLEFEM\n");
    TetMesh * tetMesh = dynamic_cast<TetMesh*>(volumetricMesh);
    if (tetMesh == nullptr)
    {
      printf("Error: the input mesh is not a tet mesh (Invertible FEM deformable model).\n");
      exit(1);
    }

    IsotropicMaterial * isotropicMaterial = nullptr;

    // create the invertible material model
    if (strcmp(invertibleMaterialString, "StVK") == 0)
      invertibleMaterial = INV_STVK;
    if (strcmp(invertibleMaterialString, "neoHookean") == 0)
      invertibleMaterial = INV_NEOHOOKEAN;
    if (strcmp(invertibleMaterialString, "MooneyRivlin") == 0)
      invertibleMaterial = INV_MOONEYRIVLIN;

    switch (invertibleMaterial)
    {
      case INV_STVK:
      {
        isotropicMaterial = new StVKIsotropicMaterial(tetMesh, enableCompressionResistance, compressionResistance);
        printf("Invertible material: StVK.\n");
        break;
      }

      case INV_NEOHOOKEAN:
        isotropicMaterial = new NeoHookeanIsotropicMaterial(tetMesh, enableCompressionResistance, compressionResistance);
        printf("Invertible material: neo-Hookean.\n");
        break;

      case INV_MOONEYRIVLIN:
        isotropicMaterial = new MooneyRivlinIsotropicMaterial(tetMesh, enableCompressionResistance, compressionResistance);
        printf("Invertible material: Mooney-Rivlin.\n");
        break;

      default:
        printf("Error: invalid invertible material type.\n");
        exit(1);
        break;
    }

    // create the invertible FEM deformable model
    IsotropicHyperelasticFEM * isotropicHyperelasticFEM = new IsotropicHyperelasticFEM(tetMesh, isotropicMaterial, inversionThreshold, addGravity, g);
    isotropicHyperelasticFEMStencilForceModel = new IsotropicHyperelasticFEMStencilForceModel(isotropicHyperelasticFEM);
    stencilForceModel = isotropicHyperelasticFEMStencilForceModel;
  }

  if (deformableObject == MASSSPRING)
  {
    printf("Force model: MASSSPRING\n");

    massSpringStencilForceModel = new MassSpringStencilForceModel(massSpringSystem);
    stencilForceModel = massSpringStencilForceModel;

    renderMassSprings = new RenderSprings();
  }

  assert(stencilForceModel != nullptr);
  forceModelAssembler = new ForceModelAssembler(stencilForceModel);
  forceModel = forceModelAssembler;

  // initialize the integrator
  printf("Initializing the integrator, n = %d...\n", n);
  printf("Solver type: %s\n", solverMethod);

  integratorBaseSparse = nullptr;
  if (solver == IMPLICITNEWMARK)
  {
    implicitNewmarkSparse = new ImplicitNewmarkSparse(3*n, timeStep, massMatrix, forceModel, numFixedDOFs, fixedDOFs,
       dampingMassCoef, dampingStiffnessCoef, maxIterations, epsilon, newmarkBeta, newmarkGamma, numSolverThreads);
    integratorBaseSparse = implicitNewmarkSparse;
  }
  else if (solver == IMPLICITBACKWARDEULER)
  {
    implicitNewmarkSparse = new ImplicitBackwardEulerSparse(3*n, timeStep, massMatrix, forceModel, numFixedDOFs, fixedDOFs,
       dampingMassCoef, dampingStiffnessCoef, maxIterations, epsilon, numSolverThreads);
    integratorBaseSparse = implicitNewmarkSparse;
  }
  else if (solver == EULER)
  {
    int symplectic = 0;
    integratorBaseSparse = new EulerSparse(3*n, timeStep, massMatrix, forceModel, symplectic, numFixedDOFs, fixedDOFs, dampingMassCoef);
  }
  else if (solver == SYMPLECTICEULER)
  {
    int symplectic = 1;
    integratorBaseSparse = new EulerSparse(3*n, timeStep, massMatrix, forceModel, symplectic, numFixedDOFs, fixedDOFs, dampingMassCoef);
  }
  else if (solver == CENTRALDIFFERENCES)
  {
    integratorBaseSparse = new CentralDifferencesSparse(3*n, timeStep, massMatrix, forceModel, numFixedDOFs, fixedDOFs, dampingMassCoef, dampingStiffnessCoef, centralDifferencesTangentialDampingUpdateMode, numSolverThreads);
  }

  integratorBase = integratorBaseSparse;

  if (integratorBase == nullptr)
  {
    printf("Error: failed to initialize numerical integrator.\n");
    exit(1);
  }

  // set integration parameters
  integratorBaseSparse->SetDampingMatrix(LaplacianDampingMatrix);
  integratorBase->ResetToRest();
  integratorBase->SetState(uInitial, velInitial);
  integratorBase->SetTimestep(timeStep / substepsPerTimeStep);

  if (implicitNewmarkSparse != nullptr)
  {
    implicitNewmarkSparse->UseStaticSolver(staticSolver);
    if (velInitial != nullptr)
      implicitNewmarkSparse->SetState(implicitNewmarkSparse->Getq(), velInitial);
  }

  // load any external geometry file (e.g. some static scene for decoration; usually there will be none)
  if (strcmp(extraSceneGeometryFilename,"__none") != 0)
  {
    extraSceneGeometry = new SceneObject(extraSceneGeometryFilename);
    extraSceneGeometry->BuildNormals(85.0);
  }
  else
    extraSceneGeometry = nullptr;

  // set up the ground plane (for rendering)
  renderGroundPlane = (strcmp(groundPlaneString, "__none") != 0);
  if (renderGroundPlane)
  {
    double groundPlaneR, groundPlaneG, groundPlaneB;
    double groundPlaneAmbient, groundPlaneDiffuse, groundPlaneSpecular, groundPlaneShininess;
    sscanf(groundPlaneString,"%lf,%lf,%lf,r%lf,g%lf,b%lf,a%lf,d%lf,s%lf,sh%lf", &groundPlaneHeight, &groundPlaneLightHeight, &groundPlaneSize, &groundPlaneR, &groundPlaneG, &groundPlaneB, &groundPlaneAmbient, &groundPlaneDiffuse, &groundPlaneSpecular, &groundPlaneShininess);
    displayListGround = glGenLists(1);
    glNewList(displayListGround, GL_COMPILE);
    RenderGroundPlane(groundPlaneHeight, groundPlaneR, groundPlaneG, groundPlaneB, groundPlaneAmbient, groundPlaneDiffuse, groundPlaneSpecular, groundPlaneShininess);
    glEndList();
  }

  // set background color
  int colorR, colorG, colorB;
  sscanf(backgroundColorString, "%d %d %d", &colorR, &colorG, &colorB);
  glClearColor(1.0 * colorR / 255, 1.0 * colorG / 255, 1.0 * colorB / 255, 0.0);

  //Sync_GLUI();
  //callAllUICallBacks();
  /*
   * ORIGINAL CODE OF MATHIAS GRYMER
   * EMAIL: mathias1292@gmail.com
   */

  if (strcmp(experimentType, "registration") == 0) {
    experimentPointClouds.LoadFromAscii(experimentFilename);
    vector<Vec3d> vec;
    experimentPointClouds.getPointCloud(vec, 0);
    printf("experimentPcloud file %s\n", experimentFilename);
    printf("v %i\n", experimentPointClouds.getNumPointClouds());
    printf("v %lu\n", vec.size());
    for (int i = 0; i < experimentPointClouds.getNumPointClouds(); i++) {
      experimentPointClouds.addGaussianNoise(i,
                                             noise_mean,
                                             noise_range,
                                             sample_size);

    }

    if (partialPointCloudOrient[0] != 0 or
        partialPointCloudOrient[1] != 0 or
        partialPointCloudOrient[2] != 0) {
      experimentPointClouds.severPointClouds(partialPointCloudCenter, partialPointCloudOrient);
    }
  }
  // END OF ORIGINAL CODE
  titleBarCounter.StartCounter();
}

// set up the configuration file
void initConfigurations()
{
  printf("Parsing configuration file %s...\n", configFilename.c_str());
  ConfigFile configFile;

  // specify the entries of the config file

  // at least one of the following must be present:
  configFile.addOptionOptional("volumetricMeshFilename", volumetricMeshFilename, "__none");
  configFile.addOptionOptional("customMassSpringSystem", customMassSpringSystem, "__none");
  configFile.addOptionOptional("deformableObjectMethod", deformableObjectMethod, "StVK");
  configFile.addOptionOptional("massSpringSystemObjConfigFilename", massSpringSystemObjConfigFilename, "__none");
  configFile.addOptionOptional("massSpringSystemTetMeshConfigFilename", massSpringSystemTetMeshConfigFilename, "__none");
  configFile.addOptionOptional("massSpringSystemCubicMeshConfigFilename", massSpringSystemCubicMeshConfigFilename, "__none");

  // option for corotational linear FEM: if warp is disabled, one gets purely linear FEM
  configFile.addOptionOptional("corotationalLinearFEM_warp", &corotationalLinearFEM_warp, corotationalLinearFEM_warp);

  configFile.addOptionOptional("implicitSolverMethod", implicitSolverMethod, "none"); // this is now obsolete, but preserved for backward compatibility, use "solver" below
  configFile.addOptionOptional("solver", solverMethod, "implicitNewmark");

  configFile.addOptionOptional("centralDifferencesTangentialDampingUpdateMode", &centralDifferencesTangentialDampingUpdateMode, centralDifferencesTangentialDampingUpdateMode);

  configFile.addOptionOptional("initialPositionFilename", initialPositionFilename, "__none");
  configFile.addOptionOptional("initialVelocityFilename", initialVelocityFilename, "__none");
  configFile.addOptionOptional("outputFilename", outputFilename, "__none");
  configFile.addOptionOptional("addGravity", &addGravity, addGravity);
  configFile.addOptionOptional("g", &g, g);

  configFile.addOptionOptional("renderingMeshFilename", renderingMeshFilename, "__none");
  configFile.addOptionOptional("secondaryRenderingMeshFilename", secondaryRenderingMeshFilename, "__none");
  configFile.addOptionOptional("secondaryRenderingMeshInterpolationFilename", secondaryRenderingMeshInterpolationFilename, "__none");
  configFile.addOptionOptional("useRealTimeNormals", &useRealTimeNormals, 0);
  configFile.addOptionOptional("fixedVerticesFilename", fixedVerticesFilename, "__none");
  configFile.addOptionOptional("enableCompressionResistance", &enableCompressionResistance, enableCompressionResistance);
  configFile.addOptionOptional("compressionResistance", &compressionResistance, compressionResistance);
  configFile.addOption("timestep", &timeStep);
  configFile.addOptionOptional("substepsPerTimeStep", &substepsPerTimeStep, substepsPerTimeStep);
  configFile.addOptionOptional("syncTimestepWithGraphics", &syncTimestepWithGraphics, syncTimestepWithGraphics);
  configFile.addOption("dampingMassCoef", &dampingMassCoef);
  configFile.addOption("dampingStiffnessCoef", &dampingStiffnessCoef);
  configFile.addOptionOptional("dampingLaplacianCoef", &dampingLaplacianCoef, dampingLaplacianCoef);
  configFile.addOptionOptional("newmarkBeta", &newmarkBeta, newmarkBeta);
  configFile.addOptionOptional("newmarkGamma", &newmarkGamma, newmarkGamma);
  configFile.addOption("deformableObjectCompliance", &deformableObjectCompliance);
  configFile.addOption("frequencyScaling", &frequencyScaling);
  configFile.addOptionOptional("forceNeighborhoodSize", &forceNeighborhoodSize, forceNeighborhoodSize);
  configFile.addOptionOptional("maxIterations", &maxIterations, 1);
  configFile.addOptionOptional("epsilon", &epsilon, 1E-6);
  configFile.addOptionOptional("numInternalForceThreads", &numInternalForceThreads, 0);
  configFile.addOptionOptional("numSolverThreads", &numSolverThreads, 1);
  configFile.addOptionOptional("inversionThreshold", &inversionThreshold, -DBL_MAX);
  configFile.addOptionOptional("forceLoadsFilename", forceLoadsFilename, "__none");

  configFile.addOptionOptional("windowWidth", &windowWidth, windowWidth);
  configFile.addOptionOptional("windowHeight", &windowHeight, windowHeight);
  configFile.addOptionOptional("cameraRadius", &cameraRadius, 17.5);
  configFile.addOptionOptional("focusPositionX", &focusPositionX, 0.0);
  configFile.addOptionOptional("focusPositionY", &focusPositionY, 0.0);
  configFile.addOptionOptional("focusPositionZ", &focusPositionZ, 0.0);
  configFile.addOptionOptional("cameraLongitude", &cameraLongitude, -60.0);
  configFile.addOptionOptional("cameraLattitude", &cameraLattitude, 30.0);
  configFile.addOptionOptional("renderWireframe", &renderWireframe, 1);
  configFile.addOptionOptional("renderSecondaryDeformableObject", &renderSecondaryDeformableObject, renderSecondaryDeformableObject);
  configFile.addOptionOptional("renderAxes", &renderAxes, renderAxes);
  configFile.addOptionOptional("extraSceneGeometry", extraSceneGeometryFilename, "__none");
  configFile.addOptionOptional("enableTextures", &enableTextures, enableTextures);
  configFile.addOptionOptional("backgroundColor", backgroundColorString, backgroundColorString);
  configFile.addOption("lightingConfigFilename", lightingConfigFilename);
  configFile.addOptionOptional("groundPlane", groundPlaneString, "__none");

  configFile.addOptionOptional("singleStepMode", &singleStepMode, singleStepMode);
  configFile.addOptionOptional("pauseSimulation", &pauseSimulation, pauseSimulation);
  configFile.addOptionOptional("lockAt30Hz", &lockAt30Hz, lockAt30Hz);

  configFile.addOptionOptional("invertibleMaterial", invertibleMaterialString, invertibleMaterialString);

  /*
   * ORIGINAL CODE OF MATHIAS GRYMER
   * EMAIL: mathias1292@gmail.com
   */
  configFile.addOptionOptional("experimentDurationInSeconds", &elapsedTime, elapsedTime);
  configFile.addOptionOptional("experimentFilename", experimentFilename, "__none");
  configFile.addOptionOptional("experimentType", experimentType, "__none");
  configFile.addOptionOptional("registrationModel", registrationModel, "__none");
  configFile.addOptionOptional("deformationPointCloudNoiseMean", &noise_mean, noise_mean);
  configFile.addOptionOptional("deformationPointCloudNoiseRange", &noise_range, noise_range);
  configFile.addOptionOptional("deformationPointCloudSampleSize", &sample_size, sample_size);
  configFile.addOptionOptional("epsilonMSE", &epsilonMSE, epsilonMSE);
  configFile.addOptionOptional("epsilonMSEmin", &epsilonMSEmin, epsilonMSEmin);
  configFile.addOptionOptional("epsilonDisplacement", &  epsilonDisplacement, epsilonDisplacement);
  configFile.addOptionOptional("maxPointCorrespondenceDistance", &max_point_correspondence_distance, max_point_correspondence_distance);
  configFile.addOptionOptional("minPointCorrespondenceDistance", &min_point_correspondence_distance, min_point_correspondence_distance);
  configFile.addOptionOptional("partialPointCloudCenter", &partialPointCloudCenter, partialPointCloudCenter);
  configFile.addOptionOptional("partialPointCloudOrient", &partialPointCloudOrient, partialPointCloudOrient);

  configFile.addOptionOptional("goalDeformationTimeStep", &goalDeformationTimeStep, goalDeformationTimeStep);
  configFile.addOptionOptional("goaldeformationstride", &goaldeformationstride, goaldeformationstride);
  configFile.addOptionOptional("automatedExperiment", automatedExperiment, "__none");
  configFile.addOptionOptional("experimentReset", &experimentReset, experimentReset);

  configFile.addOptionOptional("forceFactor", &forceFactor, forceFactor);
  // Additional information about target callbackFunctionality for synthetic deformation
  configFile.addOptionOptional("targetFunction", targetFunction, targetFunction);
  configFile.addOptionOptional("targetCenterPos", &targetCenterPos1, targetCenterPos1);
  configFile.addOptionOptional("targetPointPos", &targetPointPos1, targetPointPos1);
  configFile.addOptionOptional("targetPlotSize", &targetPlotSize, targetPlotSize);
  configFile.addOptionOptional("surfaceTargetPoint", &surfaceTargetPoint, surfaceTargetPoint);
  configFile.addOptionOptional("experimentPlotSize", &experimentPlotSize, experimentPlotSize);

  configFile.addOptionOptional("targetAxis1", &axis1, axis1);
  configFile.addOptionOptional("targetAxis2", &axis2, axis2);
  configFile.addOptionOptional("circularRadius", &r, r);


  configFile.addOptionOptional("tipPosIndex", &tipPosIndex, tipPosIndex);
  configFile.addOptionOptional("experimentDataFilename", experimentDataFileName, "__none");
  // END OF ORIGINAL CODE


  // parse the configuration file
  if (configFile.parseOptions((char*)configFilename.c_str()) != 0)
  {
    printf("Error parsing options.\n");
    exit(1);
  }

  // the config variables have now been loaded with their specified values

  // informatively print the variables (with assigned values) that were just parsed
  configFile.printOptions();

  // set the solver based on config file input
  solver = UNKNOWN;
  if (strcmp(implicitSolverMethod, "implicitNewmark") == 0)
    solver = IMPLICITNEWMARK;
  if (strcmp(implicitSolverMethod, "implicitBackwardEuler") == 0)
    solver = IMPLICITBACKWARDEULER;

  if (strcmp(solverMethod, "implicitNewmark") == 0)
    solver = IMPLICITNEWMARK;
  if (strcmp(solverMethod, "implicitBackwardEuler") == 0)
    solver = IMPLICITBACKWARDEULER;
  if (strcmp(solverMethod, "Euler") == 0)
    solver = EULER;
  if (strcmp(solverMethod, "symplecticEuler") == 0)
    solver = SYMPLECTICEULER;
  if (strcmp(solverMethod, "centralDifferences") == 0)
    solver = CENTRALDIFFERENCES;

  if (solver == UNKNOWN)
  {
    printf("Error: unknown implicit solver specified.\n");
    exit(1);
  }
}

// GLUI-related functions
void Sync_GLUI()
{
  glui->sync_live();
}

void deformableObjectCompliance_spinnerCallBack(int code)
{
  if (deformableObjectCompliance < 0)
    deformableObjectCompliance = 0;

  glui->sync_live();
}

void timeStep_spinnerCallBack(int code)
{
  if (timeStep < 0)
    timeStep = 0;

  integratorBase->SetTimestep(timeStep / substepsPerTimeStep);

  glui->sync_live();
}

void syncTimestepWithGraphics_checkboxCallBack(int code)
{
  if (syncTimestepWithGraphics)
    timeStep_spinner->disable();
  else
    timeStep_spinner->enable();
}

void frequencyScaling_spinnerCallBack(int code)
{
  if (frequencyScaling < 0)
    frequencyScaling = 0;

  glui->sync_live();

  integratorBase->SetInternalForceScalingFactor(frequencyScaling * frequencyScaling);
}

void newmarkBeta_spinnerCallBack(int code)
{
  if (newmarkBeta < 0)
    newmarkBeta = 0;

  if (newmarkBeta > 0.5)
    newmarkBeta = 0.5;

  if (use1DNewmarkParameterFamily)
  {
    if (newmarkBeta > 0.25)
      newmarkGamma = sqrt(4.0 * newmarkBeta) - 0.5;
    else
      newmarkGamma = 0.5;
  }

  if (implicitNewmarkSparse != nullptr)
  {
    implicitNewmarkSparse->SetNewmarkBeta(newmarkBeta);
    implicitNewmarkSparse->SetNewmarkGamma(newmarkGamma);
  }

  glui->sync_live();
}

void newmarkGamma_spinnerCallBack(int code)
{
  if (newmarkGamma < 0.5)
    newmarkGamma = 0.5;

  if (newmarkGamma > 1.0)
    newmarkGamma = 1.0;

  if (use1DNewmarkParameterFamily)
    newmarkBeta = (newmarkGamma + 0.5) * (newmarkGamma + 0.5) / 4.0;

  if (implicitNewmarkSparse != nullptr)
  {
    implicitNewmarkSparse->SetNewmarkBeta(newmarkBeta);
    implicitNewmarkSparse->SetNewmarkGamma(newmarkGamma);
  }

  glui->sync_live();
}

void newmark_checkboxuse1DNewmarkParameterFamilyCallBack(int code)
{
  if (use1DNewmarkParameterFamily)
  {
    newmarkBeta = (newmarkGamma + 0.5) * (newmarkGamma + 0.5) / 4.0;

    if (implicitNewmarkSparse != nullptr)
    {
      implicitNewmarkSparse->SetNewmarkBeta(newmarkBeta);
      implicitNewmarkSparse->SetNewmarkGamma(newmarkGamma);
    }
  }
  glui->sync_live();
}

void rayleighMass_spinnerCallBack(int code)
{
  if (dampingMassCoef < 0)
    dampingMassCoef = 0;

  integratorBase->SetDampingMassCoef(dampingMassCoef);

  glui->sync_live();
}

void rayleighStiffness_spinnerCallBack(int code)
{
  if (dampingStiffnessCoef < 0)
    dampingStiffnessCoef = 0;

  integratorBase->SetDampingStiffnessCoef(dampingStiffnessCoef);

  glui->sync_live();
}

void laplacianDamping_spinnerCallBack(int code)
{
  glui->sync_live();
}

void timeStepSubdivisions_spinnerCallBack(int code)
{
  if (substepsPerTimeStep < 1)
    substepsPerTimeStep = 1;

  integratorBase->SetTimestep(timeStep / substepsPerTimeStep);

  glui->sync_live();
}

void stopDeformations_buttonCallBack(int code)
{
  integratorBase->ResetToRest();
  integratorBase->SetState(uInitial);
  memcpy(u, integratorBase->Getq(), sizeof(double) * 3 * n);
  deformableObjectRenderingMesh->SetVertexDeformations(u);
  timestepCounter = 0;
  subTimestepCounter = 0;
}

void staticSolver_checkboxCallBack(int code)
{
  implicitNewmarkSparse->UseStaticSolver(staticSolver);
}

void exit_buttonCallBack(int code)
{
  exit(0);
}

// calls the GLUI callbacks
void callAllUICallBacks()
{
  deformableObjectCompliance_spinnerCallBack(0);
  frequencyScaling_spinnerCallBack(0);
  timeStep_spinnerCallBack(0);
  syncTimestepWithGraphics_checkboxCallBack(0);
  rayleighMass_spinnerCallBack(0);
  rayleighStiffness_spinnerCallBack(0);
  laplacianDamping_spinnerCallBack(0);
  timeStepSubdivisions_spinnerCallBack(0);
  newmarkBeta_spinnerCallBack(0);
  newmarkGamma_spinnerCallBack(0);
  newmark_checkboxuse1DNewmarkParameterFamilyCallBack(0);
}

// main function
int main(int argc, char* argv[])
{
  int numFixedArgs = 2;
  if ( argc < numFixedArgs ) 
  {
    printf("Real-time deformable object simulator.\n");
    printf("Usage: %s [config file]\n", argv[0]);
    return 1;
  }

  // parse command line options
  char * configFilenameC = argv[1];
  opt_t opttable[] =
  {
    { nullptr, 0, nullptr }
  };

  argv += (numFixedArgs-1);
  argc -= (numFixedArgs-1);
  int optup = getopts(argc,argv,opttable);
  if (optup != argc)
  {
    printf("Error parsing options. Error at option %s.\n",argv[optup]);
  }

  printf("Starting application.\n");

  configFilename = string(configFilenameC);
  printf("Loading scene configuration from %s.\n", configFilename.c_str());
  startTime = ((float) clock())/CLOCKS_PER_SEC;
  initConfigurations(); // parse the config file
  initGLUT(argc, argv, windowTitleBase , windowWidth, windowHeight, &windowID);
  initGraphics(windowWidth, windowHeight); // more OpenGL initialization calls

  initSimulation(); // init the simulation
  glutMainLoop(); // you have reached the point of no return..

  return 0;
}

