#!/usr/bin/env python3.7

#####################################################################################
# This script is an automated simulation driver for running a multiple setup tests. #
# Intended to ease the synthetic data generation                                    #
#####################################################################################


import sys
import os
import subprocess

def construct_path(S, N, s):
    
    return ("/Users/NewUser/Documents/experimentdata/syntheticExperimentS" + 
            str(S) + "N" + str(int(N*10)) + "S" + str(int(s*10)) +"/\n")

def construct_config(config, S, N, s):
    stride = "*goaldeformationstride\n"
    noise_range = "*deformationPointCloudNoiseRange\n"
    sample_size = "*deformationPointCloudSampleSize\n"
    path = "*experimentDataFileName\n"
    
    config += "\n" + stride + str(S) + "\n"
    config += "\n" + noise_range + str(N) + "\n"
    config += "\n" + sample_size + str(s) + "\n"
    experiment_data_path = construct_path(S,N,s)
    if not os.path.isdir(experiment_data_path[:-1]):
        os.mkdir(experiment_data_path[:-1])
    config += "\n" + path + experiment_data_path + "\n"
    return config

if __name__ == "__main__":
    S = [1, 5, 10]
    N = [0.0, 0.5, 1.0, 2.0, 3.0, 4.0]
    s = [0.5, 1.0, 2.0]
    

    config_path = "synthetic_automated_registration.config"
    
    with open(config_path) as f:
        config_content = f.read()

    command = "../../../bin/softRoboticsSimulator"
    tmp_config_file = "tmp.config"
    

    # Iterate over strides
    for i in S:
        # Iterates over noise range
        for j in N:
            # Iterate over sampling size
            for c in s:
                #Construct config file
                tmp_config_content = construct_config(config_content, i, j, c)
                # Store config file at temporary address
                with open(tmp_config_file, 'w+') as f:
                    f.write(tmp_config_content)
                # Run simulation for current tmp config
                os.system(command + " " + tmp_config_file)



